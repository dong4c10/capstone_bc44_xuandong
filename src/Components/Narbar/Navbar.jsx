import React, { Component } from "react";
import ReactDOM from "react-dom";
import "./Navbar.css"

export default class Navbar extends Component {
  state = {
    isMenuOpen: false,
  };

  toggleMenu = () => {
    this.setState({
      isMenuOpen: !this.state.isMenuOpen,
    });
  };

  render() {
    return (
      <nav id="navbar" class="navbar navbar-light bg-light">
        <div class="container">
          <a class="navbar-brand" href="#">
            BLOCKTER
          </a>
          <ul class="nav nav-pills">
            <li class="nav-item">
              <a class="nav-link" href="#">
                Home
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                My Account
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                Movies
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">
                Questions
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}

// ReactDOM.render(<Navbar />, document.getElementById("root"));
