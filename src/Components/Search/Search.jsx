import React from "react";
import "./search.css";
import SimpleSlider from "../Slick/Slick";
export default function Search() {
  return (
    <div>
      <form className="flex header-search-form ">
        <select name="topsortby" className="search-movies">
          <option value="ht_movie">Movie</option>
          <option value="cast">Cast</option>
          <option value="news">News</option>
        </select>
        <input
          required=""
          className="header-search-form-input search-autocomplete relative"
          name="s"
          value=""
          type="text"
          placeholder="Search for Movie"
          autoComplete="off"
        />
        <button className="absolute button-search border-none">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            class="bi bi-search"
            viewBox="0 0 16 16"
          >
            <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
          </svg>
        </button>
      </form>
      <SimpleSlider />
    </div>
  );
}
