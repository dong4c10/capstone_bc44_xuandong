import React, { Component, useEffect } from "react";
import Slider from "react-slick";
import "./Slick.css";
import { useState } from "react";
import { https } from "../../Services/config";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function SimpleSlide() {
  const [movieArr, setMovieArr] = useState([]);
  console.log("movieArr: ", movieArr);
  useEffect(() => {
    https
      .get("api/QuanLyPhim/LayDanhSachPhim?maNhom=GP08")
      .then((res) => {
        // console.log("res: ", res);
        setMovieArr(res.data.content);
      })
      .catch((error) => {
        console.log("error: ", error);
      });
  }, []);
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 2,
  };
  let renderMovieList = () => {
    return movieArr.map(({ hinhAnh, tenPhim, maPhim }) => {
      //listmovie
      return (
        <div key={maPhim} className="w-full h-3/4 rounded-2xl relative ">
          {/* <div className="w-full h-3/4 rounded-2xl relative"> */}
          <div className="list_slick">
            <div className="boxx__header  relative">
              <a>
                <div className="img__top absolute text-center m-3 z-0">
                  <img
                    className="rounded-2xl h-96 w-80 object-fill"
                    src={hinhAnh}
                    alt=""
                  />
                  <h2 className="card_meta absolute mt-10">{tenPhim}</h2>
                </div>
                <div className="content__bottom absolute z-10 h-12">
                  <span className=" bg-red-700 rounded-3xl font-extrabold px-3 py-2 text-white flex items-center">
                    <a>READ MORE</a>
                  </span>
                </div>
              </a>
            </div>
          </div>
          
          {/* </div> */}
        </div>
      );
    });
  };
  return (
    <div className="container">
      <div className="mt-24">
        <Slider className="" {...settings}>
          {renderMovieList()}
        </Slider>
      </div>
    </div>
  );
}
