import React from "react";
import "./Header.css";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";
export default function Header() {
  return (
    <div className="headerColor ">
      <div className="">
        <div className="justify-between space-y-5 items-center">
          <div className="">
            <div className="navbar headerColor flex-nowrap">
              <div className="navbar-start">
                <div className="dropdown flex lg:block items-center">
                  <label tabIndex={0} className="btn btn-ghost lg:hidden">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-5 w-5"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                        d="M4 6h16M4 12h8m-8 6h16"
                      />
                    </svg>
                  </label>
                  <ul
                    tabIndex={0}
                    className="menu menu-sm dropdown-content mt-56 z-[1] p-2 shadow bg-base-100 rounded-box w-52"
                  >
                    <li>
                      <a>Item 1</a>
                    </li>
                    <li>
                      <a>Parent</a>
                      <ul className="p-2">
                        <li>
                          <a>Submenu 1</a>
                        </li>
                        <li>
                          <a>Submenu 2</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a>Item 3</a>
                    </li>
                  </ul>
                  <h1 className="title_menu text-red-500 xl:text-3xl shadow font-bold ml-2 sm:text-sm">
                    <NavLink to="/">Cyber NetFlix</NavLink>
                  </h1>
                </div>
              </div>
              <div className="relative z-10">
                <nav className="navbar-center hidden p-3 lg:flex mr-24">
                  <ul className="navbar-nav ml-auto menu-horizontal h-9 gap-0">
                    <li className="nav-item dropdown font-extrabold text-gray-500">
                      <a
                        className="nav-link  dropdown-toggle"
                        href="#"
                        data-toggle="dropdown"
                      >
                        HOME
                      </a>
                      <ul className="dropdown-menu dropdown-menu-right fade-down py-4 px-1">
                        <li className="">
                          <a
                            className="dropdown-item font-extrabold text-gray-500"
                            href="#"
                          >
                            HOME MOVIE 1
                          </a>
                        </li>
                        <li className="mt-2">
                          <a
                            className="dropdown-item font-extrabold text-gray-500"
                            href="#"
                          >
                            HOME MOVIE 2
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li className="nav-item dropdown font-extrabold text-gray-500">
                      <a
                        className="nav-link  dropdown-toggle"
                        href="#"
                        data-toggle="dropdown"
                      >
                        MOVIES
                      </a>
                      <ul className="dropdown-menu dropdown-menu-right fade-down py-4 px-1 ">
                        <li>
                          <a
                            className="dropdown-item font-extrabold text-gray-500 "
                            href="#"
                          >
                            MOVIE LIST
                          </a>
                        </li>
                        <li className="mt-2">
                          <a
                            className="dropdown-item font-extrabold text-gray-500"
                            href="#"
                          >
                            MOVIE GIRD
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li className="nav-item dropdown font-extrabold text-gray-500">
                      <a
                        className="nav-link  dropdown-toggle"
                        href="#"
                        data-toggle="dropdown"
                      >
                        CELEBRITIES
                      </a>
                      <ul className="dropdown-menu dropdown-menu-right fade-down py-4 px-1">
                        <li>
                          <a
                            className="dropdown-item font-extrabold text-gray-500"
                            href="#"
                          >
                            CELEBRITIES LIST
                          </a>
                        </li>
                        <li className="mt-2">
                          <a
                            className="dropdown-item font-extrabold text-gray-500"
                            href="#"
                          >
                            CELEBRITIES GIRD
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li className="nav-item dropdown font-extrabold text-gray-500">
                      <a
                        className="nav-link  dropdown-toggle"
                        href="#"
                        data-toggle="dropdown"
                      >
                        NEWS
                      </a>
                      <ul className="dropdown-menu dropdown-menu-right fade-down py-4 px-1">
                        <li>
                          <a
                            className="dropdown-item font-extrabold text-gray-500"
                            href="#"
                          >
                            BLOG LIST
                          </a>
                        </li>
                        <li className="mt-2">
                          <a
                            className="dropdown-item font-extrabold text-gray-500"
                            href="#"
                          >
                            BLOG GIRD
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </nav>
              </div>

              <UserNav />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
