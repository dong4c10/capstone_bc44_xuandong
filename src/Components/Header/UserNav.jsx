import { Space } from "antd";
import React from "react";
import "./Header.css";
import { useSelector } from "react-redux";
import { localServ } from "../../Services/LocalStoreService";

export default function UserNav() {
  let user = useSelector((state) => state.spinnerSlice.userInfo);
  console.log("user: ", user);
  let btnClass = "px-5 py-2 mr-2 rounded border border-black";
  let handleLogout = () => {
    localServ.removeUser();
    window.location.reload();
  };
  let renderContent = () => {
    if (user) {
      return (
        <div className="navbar-end">
          <Space>
            <span className="lg:px-5 lg:py-2  px-3 py-1 whitespace-nowrap text-white  font-bold text-xs lg:text-base ">
              {user.hoTen}
            </span>
            <button
              onClick={handleLogout}
              className="lg:px-5 lg:py-2 px-3 py-1 whitespace-nowrap text-white font-bold rounded-2xl  bg-red-600 text-xs lg:text-base"
            >
              Đăng Xuất
            </button>
          </Space>
        </div>
      );
    } else {
      return (
        <div className="navbar-end">
          <Space>
            <button
              onClick={() => {
                window.location.href = "./login";
              }}
              className="lg:px-5 lg:py-2  px-3 py-1 whitespace-nowrap text-white  font-bold text-xs lg:text-base "
            >
              Đăng Nhập
            </button>
            <button className="lg:px-5 lg:py-2 px-3 py-1 whitespace-nowrap text-white font-bold rounded-2xl  bg-red-600 text-xs lg:text-base">
              Đăng Ký
            </button>
          </Space>
        </div>
      );
    }
  };
  return (
    <div className="flex items-center space-x-5">
      {/* <Space>
        <button className="lg:px-5 lg:py-2  px-3 py-1 whitespace-nowrap text-white  font-bold text-xs lg:text-base ">
          <p className=" delay-75">Đăng Nhập</p>
        </button>
        <button className="lg:px-5 lg:py-2 px-3 py-1 whitespace-nowrap text-white font-bold rounded-2xl  bg-red-600 text-xs lg:text-base">
          <p className=" delay-75">Đăng Ký</p>
        </button>
      </Space> */}
      {renderContent()}
    </div>
  );
}
