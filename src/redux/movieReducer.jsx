import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
};

const movieReducer = createSlice({
  name: "movieReducer",
  initialState,
  reducers: {
    batLoading: (state, action) => {
      state.isLoading = true;
    },
    tatLoading: (state, action) => {
      state.isLoading = false;
    },
  },
});

export const { batLoading, tatLoading} = movieReducer.actions;

export default movieReducer.reducer;
