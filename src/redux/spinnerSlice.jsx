import { createSlice } from "@reduxjs/toolkit";
import { localServ } from "../Services/LocalStoreService";

const initialState = {
  userInfo: localServ.getUser(),
};

const spinnerSlice = createSlice({
  name: "spinnerSlice",
  initialState,
  reducers: {
    setLogin: (state, action) => {
      state.userInfo = action.payload;
    },
  },
});

export const { setLogin } = spinnerSlice.actions;

export default spinnerSlice.reducer;
