import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import "./App.css";
import Home from "./Pages/HomePage/Home";
import Register from "./Pages/ResigterPage/Register";
import Login from "./Pages/LoginPage/Login";
import DetailPage from "./Pages/DetailMovie/DetailPage";
import BookingPage from "./Pages/BookingPage/BookingPage";
import NotFoundPage from "./Pages/404Page/NotFoundPage";
import Layout from "./Layout/Layout";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout contentPage={<Home/>}/>}></Route>
          <Route path="/resgister" element={<Layout contentPage={<Register/>}/>}></Route>
          <Route path="/login" element={<Layout contentPage={<Login/>} />}></Route>
          <Route path="/detail" element={<Layout contentPage={<DetailPage/>} />}></Route>
          <Route path="/booking" element={<Layout contentPage={<BookingPage/>} />}></Route>
          <Route path="/404" element={<NotFoundPage/>}></Route>
          <Route path="*" element={<Navigate to={"/404"}/>}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
