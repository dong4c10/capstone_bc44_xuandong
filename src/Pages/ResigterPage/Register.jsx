import { faLock } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import React from "react";
import "./Resigter.css";
import { https } from "../../Services/config";
import { message } from "antd"; // Thêm thư viện message (vd: antd)

export default function Register() {
  const SignupForm = () => {
    const initialValues = {
      username: "",
      hoTen: "",
      email: "",
      password: "",
    };

    const validationSchema = Yup.object({
      username: Yup.string().required("Tên tài khoản là bắt  buộc"),
      hoTen: Yup.string().required("Họ Tên là bắt buộc"),
      email: Yup.string()
        .email("Địa chỉ email không hợp lệ")
        .required("Email là bắt buộc"),
      password: Yup.string()
        .min(6, "Mật khẩu phải có ít nhất 6 ký tự")
        .required("Mật khẩu là bắt buộc"),
    });

    const handleSubmit = (values, { setSubmitting }) => {
      https
        .post("/api/QuanLyNguoiDung/DangKy", values)
        .then((res) => {
          console.log("res: ", res.data.content);
          message.success("Đăng ký thành công");
          // Xử lý chuyển hướng người dùng sau khi đăng ký thành công
          // Ví dụ: history.push('/success'); hoặc window.location.href = '/success';
        })
        .catch((error) => {
          console.log("error: ", error);
          message.error("Đăng ký thất bại");
        })
        .finally(() => {
          setSubmitting(false);
        });
    };

    return (
      <div className="Main_login flex items-center  h-screen">
        <div className="container_one p-10 bg-white rounded">
          <div className="m-auto ">
            <div className=" bg-orange-600 flex text-center items-center m-auto justify-center w-24 h-24 rounded-full">
              <FontAwesomeIcon className="text-5xl" icon={faLock} />
            </div>
            <div className="text-black mt-6 text-2xl text-center">
              <h2 className="font-bold ">Đăng Ký</h2>
            </div>
          </div>

          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}
          >
            {({ isSubmitting }) => (
              <Form className="text-left">
                <div className="mt-5">
                  <label className="text-black">Tài Khoản:</label>
                  <Field type="text" name="username" />
                  <ErrorMessage
                    name="username"
                    component="div"
                    className="error "
                  />
                </div>
                <div className="mt-1">
                  <label className="text-black">Họ Tên:</label>
                  <Field type="text" name="hoTen" />
                  <ErrorMessage
                    name="hoTen"
                    component="div"
                    className="error"
                  />
                </div>
                <div className="mt-1">
                  <label className="text-black">Email:</label>
                  <Field type="email" name="email" />
                  <ErrorMessage
                    name="email"
                    component="div"
                    className="error"
                  />
                </div>
                <div className="mt-">
                  <label className="text-black">Mật Khẩu:</label>
                  <Field type="password" name="password" />
                  <ErrorMessage
                    name="password"
                    component="div"
                    className="error"
                  />
                </div>
                <div className="mt-2 text-right">
                  <button type="submit" disabled={isSubmitting}>
                    {isSubmitting ? "Đang xử lý..." : "Đăng ký"}
                  </button>
                  <div className="mt-2">
                    <a className="text-slate-800 " href="">
                      Quên Mật Khẩu?
                    </a>
                  </div>
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    );
  };

  return (
    <div className="text-center">
      <SignupForm />
    </div>
  );
}
