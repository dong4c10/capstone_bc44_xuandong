import { Divider } from "antd";
import React from "react";
import "./Content.css";

export default function Content() {
  return (
    <div className="main_content lg:flex">
      <div className="content_left w-2/3 text-left">
        <div className="flex content_one">
          <div>
            <h2 className="text-2xl">LATEST NEWS</h2>
            <p className="mt-5">
              <a className="text-blue-400" href="">
                Benaughty shall be various other regional casual dating
                situation online
              </a>
            </p>
            <p className="mt-2 font-thin">11 months ago</p>
            <p className="mt-3 font-thin">
              Benaughty shall be various other regional casual dating situation
              on line Bens having cell phones and machines, and you can
              blackberrys. It online dating web site will allow
            </p>
          </div>
          <div className="p-0 lg:p-14"></div>
        </div>
        <div className="empty_space h-24"></div>
        <div className="flex flex-wrap justify-between">
          <p>More news on Blockbuster</p>
          <p className="font-thin mt-9 lg:mt-0">SEE ALL MOVIES NEWS</p>
        </div>
        <div className="list_content mt-10">
          <div className="item_content grid grid-cols-1 lg:grid-cols-2 gap-10 mb-14 ">
            <article>
              <h2>
                <p className="text-base text-blue-500">
                  Benaughty shall be various other regional casual dating
                  situation on line
                </p>
                <p className="text-xs font-thin">11 months ago</p>
              </h2>
            </article>
            <article>
              <h2>
                <p className="text-base text-blue-500">
                  If you value which relationship, I might strongly recommend
                  guidance first
                </p>
                <p className="text-xs font-thin">11 months ago</p>
              </h2>
            </article>
            <article>
              <h2>
                <p className="text-base text-blue-500">
                  Maniera funzionano i siti di annunci in coppie? L’abbiamo
                  invocato verso chi li usa
                </p>
                <p className="text-xs font-thin">11 months ago</p>
              </h2>
            </article>
            <article>
              <h2>
                <p className="text-base text-blue-500">
                  Ma copine est petite elle cherche votre homme. Mes hommes qui
                  craignent de ne point etre a la hauteur
                </p>
                <p className="text-xs font-thin">11 months ago</p>
              </h2>
            </article>
          </div>
        </div>
      </div>
      <div className="content_right w-1/3 flex ">
        <Divider
          className="content_hr bg-white h-96 w-1 mx-10"
          type="vertical"
        />

        <p className="content_p text-3xl font-thin w-64 italic text-left">
          WordPress Names 5.5 Release Leads, Plans All-Women Release Squad for
          5.6
          <a className="text-xl mt-10" href="">
            https://t.co/xszfIkZ8mX
          </a>
          <div className="mt-10">
            <h2>— wptavern (@wptavern) </h2>
            <a className="text-lg" href="">
              June 1, 2020
            </a>
          </div>
        </p>
      </div>
    </div>
  );
}
