import { Card, Divider, Pagination } from "antd";
import React, { useEffect, useState } from "react";
import "./ListMovie.css";
import Content from "../Content/Content";
import { https } from "../../../Services/config";
import Meta from "antd/es/card/Meta";

export default function ListMovie() {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    https
      .get("api/QuanLyPhim/LayDanhSachPhim?maNhom=GP08")
      .then((res) => {
        console.log("res: ", res);
        setMovieArr(res.data.content);
      })
      .catch((error) => {
        console.log("error: ", error);
      });
  }, []);
  const numEachPage = 4; // Use a constant here to keep track of the number of cards per page

  const [minValue, setMinValue] = useState(0);
  const [maxValue, setMaxValue] = useState(numEachPage);

  let data = [
    {
      title: "Doraemon Movie 2023: Nobita và vùng đất lý tưởng trên bầu trời",
      img: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSExMWFhUXGRgbFxgYGSIbGhsgHx0dHR4dHx8aICggHx0lHRgYITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGxAQGy8lICYtLS0tMC0tLS8vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAQsAvQMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAFBgMEAQIHAAj/xABHEAABAwIDBgQDBQYFAgMJAAABAgMRACEEEjEFBiJBUWETcYGRMqGxFEJSwfAHIzNictGCkqLh8RVDJDTDJURTZHOTo7LC/8QAGgEAAgMBAQAAAAAAAAAAAAAAAwQBAgUABv/EADwRAAEDAgQCCAUDAwIHAQAAAAECAxEAIQQSMUFRYRNxgZGhscHwBRQi0eEyQvFSYpKC8hUjcqLC0uIG/9oADAMBAAIRAxEAPwBGrNer1emis+vV6vEViorqzWK9WyEkkAAknQDU10V1a1u2gqISkEk6ACSfICmnY+5TixneORIuRI+Z0FTv7dYw/wC7wTQUs2zBJg+X3lWnpSasYlSsjIznloOtWg8apmnQW4/bjQ3B7n4lYBVkbB/GqFf5RJ9KuO7sYZmPtOMSk2lKEyr2Eq9SK89s15xIdxuIDCCTlSr4j1ypH/NVmMRhkqjC4VT6h956488gt7xQx8y5q52ISI/zXPgKY6LKJchPXr3C/fA51ZwmzcIuzOGxj97KgISfNQNuR0FX0bJw6I8XDMN//VxZn1ShKu3PrWuNaxikBzGYsYdo6NosY6BKLn50DRicOFZMPhS8s/felRPcNoMaxqT5VIwylC61f5K8wUp8DUdI0DYE9w9CfGmJS9lJ4VJw5VrwKeUPKQn5VXGB2ev4GVH+j7Qf/TNZbwmLbTnxOJawKfuoShPiH+lDYBjzNQt4pDmYtt4vFRdTjzxabHogwB5qmpbyxKFEjjmJHeYT3GoVO4j33+FQubttGSkPpj8QWB/qZH1oZidiBP8A3PcD+4+lWMVj2gZKWJ6Nt+If/uOmJ7gGqS9tL+4Mo/X4AkfKm0BVLKzbVqnZSvuqSfU/2rKtmui/hqPlf6VErabp1WflWyNovcnFDyMfSi3oRKjUJkGOfStk1YO0XlWUsqH84Cv/ANgav4V5pX8Vkf1NkoV7GUHygVBJG1dloeywTerKVhOmtFxsxtxP/h3gY1S4Mqh6iZ9ooZidmPIutBAOihxJP+JMj51QLBMUQWFatrJq+yqBQ9sVbTUEVYGlarGCMONnotP1FQVmKIRIIqxNGd9MGGsdiEJ+EuFQ8lgLj0zEelBqJbbUCWSOeHanzlX5RVXBYRTq0toF1GPLuY5DWhsfSykqOgEnqFz4TQ0vBwZ9JvVvdzB+NiEtxKVBSVH8IUCgK9FKSfMU34nD4bZbcEhx5QEpBGYnWT+BHzPfUbYrCM4DCKSAVPvDKnktVrqEaJSTI6mPOpN3t1kqWvG4paVNqUVpK1WAkwVTY2ygA2HtWMtxOMJWokNaRoXI8kye2OOjbbZWckX1nYDn4R10KRs7aG1ClSzkakZU3CB3COdp4jr1opjsZhNmQxhkB7FaKUb5Z6mNdOEetVN4t8nXlnDYKUN/DmT8a+pBHwp8r9+Vabv7rJQkOvWKZJmyU9zznnFPoahICwEp2SLd9WcxCGzDV1cT6cKqYLYz+Nc8fErJnQc/IDRKdbCrOO28zgwWsIhCnLgqiUoPqONXeYFD9u7aViVDDYZs5JgACVOEaGOQtMD1o/srYTGzWfteMyqfuWWpsDeBb4joZi3zqcQ+hoALuT+lA1P448Ou1KtoUsye00N2Xug/iAcVjXFNtkZipZ41DrB+ER19oohg9owFM7MbS00mfFxTnzMn5T7CqKn3toE4nGLLeEbnhFgeyep04j6UB27tovQ2geHh0fw2hYf1KjVR+XzIA04+qHSDGoH6E8v71ddhvwpoFLSc2k6cTz/tT1XO1X8ZtPDNKJSDjHvvPPyW5/lRMq81GPOhqFP41zKXQTqEqUEIFieFIsIAOgoVFSIWUkFJII0IMEeorSSyEi2vE3/AHIAUst4qsdOAt+e+aK4jdjEoClKbICElS5gQApxPrdo/5k8iDWcVuy+2kqWAlKc2ZRnLYtCQQLgl5ABAg3OgJoUp9ZsVqPO6j0j6W8q3+1ua+Iv/ADHv37n3NTlcnUdxoeZPPvq/hNgOLjKpBCgopMniAX4ci2mcgXg84qfD7uYlRKQ0ZSlCiCQIC0laTfskg9DY1LhBhgyn/wAQ4lRLWZImEZlKS8QAIJCAmOZmKt4UYTLxvuBXOFKVHErMJyAEFBBFrqUoUIuLH+0/epyp9kfaoXt3HGzKinLClZp5JCCoxc6uJFSYjdrECBCBOWBmH3lKSL6C6FXPQda2wbmGVlU7iXULyoJhRPEoLDgnLIgJa6zmjlIylOGVmJxLieJYgFRsAotwMt+KJk6qPnVM7g/2nwqcqTp51AnYGIQtCVQhSpiSREZrEgQDwLtrairGNU2UoxEoUpKVJWOaVCxWn7w+doodtXwUhP2d5RykyklVrJhSZA1JWDpoLcyMLhVqSfMzzn6kn1ogSpwfV5QaCpQRp5zTO/gUKhS0BIV8LrXwHzTpPt0E1qdiuD+HC09U29CCRB7VS2RiltWB4Tqnkf18+cimDD4QOjM3I6gArjpYQU+Vx05gAXmbsTA7x+OwxRW3Ur2v3fz235muXVsBWBUjSJtT1hrVFLip8UrMlB/lyD0im/c5eBY/i4gB1UZidE/yyR3uedVNzdnNOLh2QlWg5nob8qObL3aw+IQv7O8lTiZ/dkg2+RF+ZEW5Vh4t5tbQYWogG0jlsbHeqttPoRLaZibHXukVecwni4pWJWtCmkDK2Z4EpHxEK0MmTm70i7Z2k64EYZtaiwBwIFgoBagFK5mwGukC1WMWl9K1YNtJ/iKCUgTEHWJgGOosZMzeiW7GzVNuLbdQfFR+IEKAVcWOoMqvR8MENReYsLbRrTr7y3UJKUQAL3mSYm/WBY3HARe3ulsINpC1CFkGSeQoZvjvCX1DDYcHwwY4dXFG36FT737fyhWEbN5/eKGn9I8tD60Z/Z1uwlps418EKykoBEZBeVeZEQehPWi4h4MI6ZYk/tHE7e+HOKAyguKyDt9+5qLZ+BZ2ThxiX058SsQEg6E3yjoBYFX96A7Mwzu0Xl4nFrPgtglR0AAvlTHIczr6mqO821XMfipQCUzkZR0E6+Z1P+1E99FDDMs4FtUWzOgczynzMqjy7Uszh1tkFZl5zU/0p3CeECw48bU4FoUlSo+hOg/qO08eJ5RQXeLbZxC8qeFhFm0CwAFgSOv0qhs3Zzj6w20kqUenLuTyFR4XDqWpKEiVKMAV1XD4VGzcPCSPFUJUryFz5DpWkopYQEIHUPU+tKSp1RWs9Z9B5AUtr3Yw2ETmxbhccifDQcqR/UrX29qD4ra7PwtYVodCUZj/AKpJ+VU9o49TqypRtMib+qupNbJIcWVugkASQiE6ecedr3qwQdVmffChlf8ATb3xrbE4Z1SvDW002oAGwSiAdJynXsZPatf+lAEAuoF7qvlHQSQL/LqRRLYOHZKlYgqbS2hX8JSjnVaQBIgzfnb2khjQ3iXv3CGkiJh1UJQPIa3OkHuNaguZTA06v5NdkkT4UAxuGaZKQFod1zhGbhvyVYEkToCBbWqTjeVSkzMEiesGKuttqdeCcoIBuGxwwLmMoi4Bg87VE4iFnPxLJkgGRJ7p1M8hVwYsdffZVSAawxhlKPCkq8qzEEpNiCQR0ip3MSUxJ4xoBYN+gtn+nnpUbTUJUVX2oggWFWkNyKnYwxmoGTBozhkgiuKiKEtoEW1qRnCk6UXw+GVEJvGpBiTz/t6VvhGMiM58h5/7CsDEJbsTrQSsnShoaCNa5iKtYRBIVA0AP+of3qsKs4N/IoK1FwR1B1FNLTmSRVl3EUXONzuMagNpngsZFuV4iP8ALR7GONtnBv4UAPlxAJSYzAwFJgctb96VtmsrcdS3BUi6hBAMDWJ5ium4XZ2Hw7CMW0ylTzY0MZiOtoGa/wAUaacq86oKbbUxlkzrxm1+o+VNN4dTjofSqwAkXMb9QkWPEVS26fs+PdeECA04AqySCQlwT1uD5mhW2d4sj6sU0W+PDlsJEKNnJBUQZBBcJB7mtC64t1DzypUtzKSk2AcFgOWVKiDVDfJ5vxfBSlPA2cxAuTIVc6mMtONYcAgKuRHVAqy3VNo+iwUVd8/+sedDd09mHF4tCFXE53Cb2FzbnJhPrXRf2m7WLGFDKLKekWiyBrbvYe9Df2VbPytrfIutWVPkNT7z7Cg37Q8V9px/htwcoCAQbTz9iSPQ0vnGK+JlJ/S0OydST2wP9NXKS1hgU6r7+Xh51n9nmASjPjHTCUJVHkBKj6RHvSptXFl95x4zxqJv05D0ECm3e5Zw2GawraoCwQs6EgRPoSb0lCtHBf8ANKsQf3WHJI079ajHQ1lw4/aJPNR18NOVNO4ODzPFyPgsPMybdwBRj9oKVIaQpSjmdVlSDyQkSf8AUU+9Tfs0SkMuSBxL5+QFGd+93/G8FanWm2mUrzZidVFP4R/L1FUcdAxAnT361CGyWLa+/SuUsYgoHCIV+LmPLpV3AbFxOIuhpxSSbry8InnJgVDiS14hGZORNgUIMEdeIz7mau4narziQhLr6gZjMsmw5ACAB2Ggi8GnDm/b40oIn6qgxAQMraWwSOeeTfU8JyhRjnMQOkVa2lgCVJyNpTKEnI2Ss9L8h6d+sVZ2bu48u5ATpGe1ryQO1tRz7UfVsnDYZGZavHWfiBUQkTzCUmSrlJPoKCt0JIi5986ulSIJJpQdbW2nJoV3yhWiRqTBgSRziMh5GoXG8jQdQZOdSFK6HKFDL5gqvrw8qJY17+IUJCElJ01jLYT5WqNGG/8AZilnm+jL/lWDRCCIzbkePGgtvB2cu0+FAE1aYNVkpqVNGImoCimrM3ovssFREUETeimzny2mearJ/M0NaTFqulxJUM1OaGC7CU/Ckf8APuaWdoElZE6W0ops/aPhNqJNyLdqBvlJMhWuuv5Us2FAmmnMirk0qCp8K1mUE9TUMUZ3ab45PK4/XrTjisqSrhQmWemdS3xI7t/CmNGGGHDToI/drAIHMOcKvnl9qI7zNZEIQBAK7kTBCRqRz1OtbSFsqbyHjCh5KiJ8wRIqHaWNzMMKIMrBVEXny5XFY7JKnRPOtv4gOiC8tgpAi39JjxCgOyovspcYcgEHKSkDkU8Qj2pW2ilTuKnm7lif50i3oVEelPOGe8PDPLUYypKR1JValvdthLz2FUQSELWFf4eNBMeav8lMrd6MLWdAD4Cax20dI1k3BSe85T5iujoYGEwRyj+E0oiZ5AnlfWuW7rMl/FoKjKs0mbzea6nvU+Rgn1JUEHIRJ6EgEW5kSB50h/s2w8OqcMxGUDl+hHzrF+FrCMC89N1HX31k0+6guYxtEWF+7+BVL9ov/m8vJKEgetz+u1KwTR3fczjnrzcenCLelApr0mETlw6B/aPKsrFrKn1k8T9vSnLcLFgKU2TEyR5xr8p9qJb07MW5hV5HVLWycxTJ4k87c41/w96RcBjC0sLHKnHA7V4S6lUEBVj31HlpVHUKCsyalpxJTCudLmw2W0FLpIWQoZQQYB1E3E87TFqN4rajjpJIuPhHTsEpgDl7dqApbg+IICTIUk/e/p+XQAjlWi8bB4ZT1zfEexPMdhAPOjEAmQJPl75UisLX9JP0nb8UdTiVE8SsxGomwjWSNf15VU2jtAEkJ0sR7f2Nhy95EnEzwpsmdJ18z+vzqQoJi3vpUpSAZND+WzJy14lbqg2m5VAj60w75EMMYfAp1Rxuf1EQAe4BNe2C61hkKfsp37s9ufkPyHal3F4kuuFxZkk360O61g7DxP4pwBLLUDU+A18d6qpTW+XpXoqxh0AmCYHXWjxQS6IrVhgkgdfkKIkZiPwpEAVPsnBeItLadVkCnvC7kttmVKLomcvw6dbzrNBdfQ3ZVUaZdfkpFtKREYZThjQDWo8bhspAjl511vD7u4a5CbT8M8I/2q27s9kH+GgT2ApA45M2BrYThFlGUkVy7fbctrDkrZUQIB8OJ58jM0F3fw0K4gReJI0610jejafgtrdMFeWG5Ghg/QAmuXsZ3JJMkzcnU6z19aI24tbWVXfRWQhl/pthtzpld2oG3TlhSIChJg5vhPpCR7ms7XS546UPAIVlkBBslJJUOI6m/IDleg+y2QrKYk85OpBiB2JiiG9GKd2e4CWPtByJIWr4ETqAgW1nUx2oSujaIPX1++NFU87ik6iNRyH3748av7ZwhOCOUFLQhSlq1X/SDc8r6W1qp+zlxvxynLEjhJN8wBg+xUPWlx/fZzFI+zqbKc1ypUzIgi/Obpjyoju5iywRmS4gOKQUOqQfDCkmQnNpPIibgmqOONrYU2o6yOGtp9ftQ28K904Um/ORfl304/tJkYKOWdM/Ohe4IhtIPMk02b2shWCeMCyQoTyuNPQkUrbpylDQnrE9Oh7zWOz9HwzoyNHIp/DCfiBPFs+YFJu9KicU+TqVq9tB8hQmj2+TRGMdB6gjyIkUC516zDmWUH+0eVecxIh5Y/uPma8AeXvyFWGMWEWBmefL25+vtULiSdTUeWi5Z1oUxRBL08RM15Dhqk2SDap0rmuioJqwDeAB7VietS4R0iYQCepqNb8kyBeuy0IuxpXnlz5VGBUyBJ0rf7PU2FCU4Va1E0yTytUqW5MCrwQAi1ulYZZgdzUZq5SbQKZtwGJxGaLIST+VdC8RKjekfdFHhBThMTaOtHX9sIjhuayMSCt2RW3ggGWPr4zTE0UkW5agGsOLBNxHrQjZu0kgGBf5+VE20KNyn3pJSCk3p5KwsWpN/aPKvAYFytRVHQJBSZ7HP/ppL2hhFMqCCCkKtPP07U1b67QCcagg/A2kGe5Kj8orx2qzjWVBwNwkCVmQUE/DfQKJFhzg96daJQ2k7b0mtsPPEKNhtx9/elTDYzw30QAEJKbe2p+dPO3sIMU2la0pzosk6ZhrB9biuQ4nZIW454rvjQoi3wwDaBp+talYOI2bkfaUvwFEZmlE5SCYkcteYuDHkYeQswsWim8NjcMlfRKG428O38U/bI2U22/9oCEJhKklKgMiVEQld7DmDNrjSotv7vpC4UXWkqQglKCFGQCoJJJJKAoZRlBItryZMC4h5pp5IlDoSCD3Gh9JB8qobn7CLAcJkpUqUE6QTPCdY4o8wYtWbjHyhCnTeBpxrULKEEBFh62FHsJihicEppQKFfZ0Ez0UiZ8swUKT9hoyp1jiHpc/X8qbnsQ22605nRF2liR8KriR0SoexoJicEWytBgfq1US90uCVtMGOYMH0pXDoj4ghf8A1DsN/vQf9pGGQC25HEpP0/5pIy10felgvYNC5u18Q5HlXOjW98LdC8Mnlasb4q0WsWoHe/p5itDWIrcit0prRrOKgK80110rAEVM4ok6RWzLGapili4ay07aDWhEmp22gApSvhSJP09pI96JYNl8wkYNeV5CVtrNzlJBzpiQTBBKZkJmaA6+howaYw+FcxAlOl7+lDmm4q2wg2qZeFKZBSSRrGg9aw2qBVyZFqS0MGpHCCdNK3YZJMm1Ro0mrez2ZOY0M/SKulZUqj2AxSRIKCbRb+wqvjkZVGJAOg/IxXkogAjWruB2aFZlqUYTBVImb6fKlCQn6qdBW5CKm2TmhIy6/wDIphxW3QiBBJvN4Fumk0nq2iQoAGwP6FTPOqcUVIQSO0/lQVs5lSqmmcT0aYTr74CgH7Q8MtGJW4oEJcKVIPIiIUPMH6ivbsbL+1bM2lh0iXCEqRGpUiVoHkVIA9a6di8K2834TqAtJ5GljC4VGzMQrwySl8FKU/hUjiF+fDm9qBnzN5N9uytMCHARv79D31xnZbwUE5JzWEfKDXTdrbNTjMN9hZU14w8FRlVgEkEgQDExF6GY/wDZot5xbiEpCVEqSoG+U3AOWDYGPSs7M2C1s9zxCVLd/AiQP8ajcj9RRCrOAkX6v49aorBpQvpVKAA4x6Se4d1dA2Lsw4bBNsuJBUlKpvaSToR/KTcVG7sfDlCVloTITBJIEACwmAKrNbzZmbiVQY/L0H5VvsTHFYLavun60hj0vN4ZxaDChuDG9GbdQ6+kKuOBq23sDDLlKmhcEcJKdfI/OlzGlxAW2viWzAUeak/cWOson/EmOdOqLEEUJ3vwlkYlCZKJS6Bzb1PmUniHketZWBfW6hTbipPM8pH5rRaKGnAoCBI07qGMKQ5h1NWKXE2PIyJB+hrl7jZSSk6gxTfhVll4sTwmXGDMgiJUgeU5h/KT0qDeXYn/ALwkyF/EOhPTt/etz4O8GnCyreCny/HWkikv/wBDhc7YxCP2khXofI9RpUqRNWU4FUwRB6VKcGAa9HIrxy1bGqoRRLZmL8FWbw0OW0WnNz5CY9xUaGJ5aVMtgRBqjn1JioacCFhVHFY9p/DL8RkQoLSUt5ERcgEZkkyLH4o9Kv7o70J+yJQhtYDSAlWYjwhBIBzTITraPu9jXP3G1pJj1vz/AFFVmcZkcSt1vO0HeNJukyI0EHmT/bWst9hIE16BjFGIAnwp3cxOJVil5FtPtMwPCcOVDiV3IzAEJUmAAbiEgVdwezVrLnh4NaFqnw1zmQ2SbXmCANSk5tY6VG27g22/FYU6FPKTlQslQygQMpVeBrxGeKiGycY4EqWkEZdbWI78qGAoJkW98rVdZQ4sE3MzYA9lxP2twpVxGKew7yGMczxE2ULocHMpX26GCLSINOYwTRSHGroIt5jUHuD+r0N3jwzW0cK64lRS+zlKUk8IIV8XktJI9OwNENi4dbLAQ5Ga8gGb6e8RPlXJcUbHaqvstRIi+9Q+FqTV3GrSwwMxJU6Jge4+tGtl4ZsJzqEqMxN48vbWg21GFFRW4QT90RZI56+X1qQ4FKjbz6qB0JabzDUi3Kdz5UG2Zsxb4KxAgxMxPYe1NDzYbShCBAAPPU86WHNokQhuSRoE6D8hrXnNoNtQH3CVm5SPu9rc/PpRXELWfIRQsM622DGu5Jt76qdMdicgK4kDpS/vdjG14Zp4atutLH9Kj4avk58qnxG0klCst+HinQ+Z5UpPS4FpBNwcvSeVukxS7TU3O1aCsRlNtK6Vu6T4cGZHzGo+selJu/yvCfCgACRaYIMXmNPeie7G9XjNJGhyCbc4v861382eF4ZDiokamBNxMdqXwj4+aLZB4HtpnHNS0VDrpJ2YpxfGVWm5m46jtTpslslKYuo6Gbx+vrXP2nvC0kidBcz0qQ75vIX+6QlEWkHMB6q4fYR51q4hGZJHvqrMwxgybV1okxfUVTxm8GHw6CHFgqNggGST0j+9ck2lv9jAhSVOC5/AnN5SkJpTVtd1avE5giNTz6/lXmGvgwZWVuq2MAcItJ9AO2tr5guwlI1Oppqx28BWsBCAlCVEtJHxJgmBm1gAx0iBRrDF5SSVBTf8yG1H/Wsgk360E2G0nCo+0vjM45dCB9b2Av7Hqa6buXtQYxklIKFCUuXmOhHcWI7E9LNhhJQM55xbvJIN6KcS5qkW0m8cIAmDuJM0mpxGJQB4bicQjmhwaeQOn+FVbKaDhKmxFpU2CJTHxC97GfSun/ZAhtRQloOLUPFlPxEkZhHMkWTPUGkzamwy06062rMtDmRwZSBmhKgAT8SYWEz6coB2niyoqRv2Sf8ATCe0AG95GmbiMKl5ASqbcLwN9ZVpeJIMQIMUDDSuVhW7OCKjH/FMmM2eUSkCx9/Kq32IixEVrB4ESK885hCFQbxQheyknUA9xr7ihmN2W0yTnZUtCgTxL4SSI0y2I8/SnPDYabCpXtlhxORaZB+XehrcmxpphspuJpTwTCHRCkZW0pCUrBzyALAyApKgLXBBAuap4javhqLcqVlJykfEZtBgwRTJjd3XUg+CpJtF5HlIgiodg7rKUP37QKkaLaN1AzZVtRpm1jyuIECb2p5cCAm57aFbv4l1TvC2opMeIlAKlEdDAPSdOVdNZ2cHAVnMkqJIBEECeYrbAAMNZW2oB0Sn6k8z8686VauOZB0TqfXX2oC1lR+m1HCUADNfwHpWuMxKGU5AVLV+FNz2noKCbQaWsZ31hpA1AN/Xv217VrtbbQaBDSQD+Ijn5ak9zSVjsW46rMtRPmdPIaCmsPhln6tPPu0FZmLxzYOUX5Cw7TqeyiW0NuJHBhU5R/8AEOp8unnr5UA8HrfzqYJqZjCrXOVKjGsCtJCEoFqyVvLcN/D7U17P2SpxtagLTHsAfXWqTuzlIMxNMOy8VlYI6L+oH9vlW7WJ8Qwb15x7EYlt1RSJQPetesbYYW2ATCjXMN29tht54KKghWcJA5EkwK6SnaqXsKWHQScoGYxJlIhQ/v2NIe3tmOIxKnVMSkEkqTMFPK/K3tFWsPvqwOEYchKUAJOaSddZA+8r2PpQQVrWpxCJNtLaaX9zwrSeaaTh0fXczrfa9tRVLZ+yVPKdQ3JSlKyVdcoMD1I+dJrrpVr7chXYd3t6MIpp1wQlTbbiyk2Pwk2HPmLTXH1psPIVqYV919Ky4nKQdKy1sttKSEma1xLfiIgm/WhjbZSoJTmzdAJv2omg1sqmXcOHUgzBoeYg5dqYFLW9kDoCXMoATa4SLkAW6kxpRXdLaKsBiQsz4S+F0DmOSh3Sb+UjnSUyuCFAwRoRTvsnFJfRldACwJB0zctPxSdOdZuJwi0DpUnMN+NbeCxTS0DDuCLQOfbx35101WyA9ikYxLysgQAhIgpVzzA/h0878q9t6FeGo3MkJg2iUzadbCgG62JxGFaUjEj9xcthSocBJ+FI1Kb84jy0x/1TO54yx/KhJMxNifOJ8pA8s155KRBNzb7+FEaw7hXa4Tp16gd+vDuo4hAWoZjygVLiNlpiReNbEn5a0Lf2s0giVEdOE/2itE7zicqApR6dfK/5VrJQ4oBTenhXm1vsNqLbxAVw37qO7OCEfDqfinp5cquvYpsAnmdQNT2tSXi94FzxMkT+JRg/5QkGqGK3gfIyheQdEgD6X+dEGBcWZPn9qXV8aw7QypkxsAR4GKc8cpA4lqS2O5En2P0of/1zDJ4UyqPQT+utIbzxJkkk8yTJq3gBzpkYFKU/USfCkf8Ai7ri/oQBzN/sPCm57eImyRFV8TiisZiRPPv7UvuvVPhMSBada7oEpEpFFGIWsws1V2iARc0FOtGceTehJiYNqcQbUi6gZqv7BDXjJ8aCg2M/KulYVtppIDaAlJvArnLisKApGUqMDKudDFxbXnyq/gsSpKZXitQABeRHI8J5EdKTxLXS3kjlf0mtDAvjDkpgHmCJHIk+EU7u7EZUkhIKD1BJ+R5Ur4d2FT86ag8Un8qrYNpDQyoTaTZXF9azkLIBBvW64wCoFNqDY7Dl5t5A1W2sDzIt864Y5MFJsRb+1fTmDaSZJA7TyrgP7RtkKwmPcBuhZ8Rs9QomZ5SFZhHl1oOEBbccnRUQOq1GcAISOFCcIs545GQb8on9edbYdGaUC5+7/avYRMJW7FkpIE9Tr9B86ubtgZy5+GI8z/t9a10EiBxnurOspSzwyjt18ARVjDbJSgZnjHaYA8z/AGqf7RhSAAlPTQ/U0N2zjPHUSDwjQD6+tDEDlSjmMIMIFq1m/hwUMzhM8tq6JgtxmsWwF4ZwJeE5klUpNzEj4kcr6dqL7l7puocBcBQpJve6fb9XoDuKFJIcSSkp0I+nl2rrzxUpLa02JsoDQ/8AEGl041agUmi4vAJYKVAzIqmzsRMZnE53LgqXc2JAMaXH15Uv7S2KRJaGgMp8unOnNThJioXcMSrMDBtPcVnJ6N2UqHUeBrk4l9lQWkzxHEVzpzEFLa+DMRBIVJ5jkIvF/SsbrbTdeUGcL4bCllSlrjMciQIy55sT8z2NbftA23h2nMrPG/8AeH/b6SqLzrpStuxjD4vEQFkgpyg8io2g5gOM3B9qvhgtpC0zI2jTXhpJ48udExa2sS82pKQFQZJFxbTNrlkbRM8q6bg3Xj4qXyXm2zxZ20pULfEmI06G+tJGMkLg2kwPWf7Uw4zHFLCkodyLdI4EkLSVWOYkglMgaFUkgdSaobTLbng3A4hnjWIi/fWm8P8AEOgF9/CNbfaszH/BvnCFWhJg85sBO1+UbC80DGtFcOAE1aO7pJCkLBSdJ1/tQ3FYlKV+DBWvNlIB066an28+us7jmltktqk7dfMVg4T4PiEPBLyDANzym8HntU7jS1ZlBMhOpFwKouPRRXZWNCEhlwpASMqkkgHiAMKB8oBjnVXA7uvPDhUjMZypKuJURmIi1pHvVcNjUrnPCY050xj/AIS6wE9GCqZm2ndy48KqKxsiDRTYmwFYgZyrIgmAYkqPOPpPamDY26zBTkcaUVEXXmMg9LHKBryo/gsJ4SUtp0QITOo86h7GADK3r7uKrhfhyiQp3Tt14HSlhO6rKVcQVlFhJgnvarLe57KuIBRB7qj0k0exjWaAYJ5dutEGnEpSE6wIpRWKdgEE1oDA4cm6B3VXeZ1UTaqCBJoq2sKt8qhxGDi40pZJ40+RU+FZGvtSL+2jYni4ROISmVMHi/oVY+ygk9hmNO+EXe5qXGFGRQcy5CCFZtCDYgz1qAopUDUKFfMWCxUtrbUdUEA9+R/vUuyHD4TgHxXj1FvpVzfXd77G+fCOZhRJbUL2/CT1GneAaobJxYTwnQ6Hp2PatNK5UM3CO+kuiCApSNyD2ihOCxkUTQ4lUQYNQ7SwBKitrKtJNwnUH01FU2GjmjQ9IisxxKm7K/mt9l4OCU/xXX908LDSDYA3KiYHufyrorWIR4SQlUi9xbtb1rlu6OCWsJQo5RaJsojsDoO5t0k2PSMXgcqBkNkgSPLp2rNfdW2hZQJI23jjHVRsYptxSBm+w5UN2vt7wSEniJuCLGO4/XPSlbbu38RiFJbZcUhJTKspiB3iCSbx5VDvWtSXgoCRkE+5oXsp4nOsCeL6CB+u9IsqOUOcQDbn/NIupKbpNxx47eNL+HwT7T/iOQ7KpOWAY/xRFrQNKZcHtRSXwtDDV/u+Gm4mdYF+9XfAQ7f4T2qFxPhXNh1rUbZcfbkmE8Rv5W4etZ7eIQlV0/VvPpXQdmrZeSCttEEXBSLHzoZtfdyVZ0qKW9SReDfURppcUv7A3xaadQ2tt4Z1BIUW4SSTFiTPyrp2JxaG0m0kAnKNbDpyqWGS19IVmT9+cmmXHDEpJSdbTte+k3APZsaSFYB1DeZLxkTHCIi2lp7SKTWMG8054iBJJmZH50/L2mxiFFtBUqQVcN8h9NRy7T50j7TxLmGeyKJIMHKRcA3E85jlTpCEmFAjqA8RFcy68tOdtQJ3CiTpOhnzI51nam08SsgQEJtIEXIGqjFzr5Vb3d2mEPIBdEyJiQn3/OlveXHJVlEWidY11t6Cg+Hx4GpPaRcf7dquj4ey4sqUo2NtJt2eA764/FcQ2yEoQJIk6xBm1la8SZ4Raa75jdr5V5VHKBBsDJFWsJjUKBXn1nLyrnWB3pCmkApKgkIRBTJJMk9x5jkkVPtDbalNk5M+URJEFPa2oHtQS26hwtkjaJHG+vVU52FMB2CIBJi/6baHnanjBoXKoVIsYMc55gCrWAcKwSOGDoaQN2t4HHIbScq8xKlGchTF5A0vlAppef8ADMEqjlluO/1B9aYKFZilQv50i2+240HUG3DcencTR/CorfGaAd6kZby2rDyQdfelt6bm9V20RrUz+GStOVYCh0NZSiRapq6d65V6S9t7DbUpTK0AtKEgTEdwdQRXKN5t1XMMFOoBW0CJWBdM6eIBp/UOH+mYrtm9roQ2lQSCuYT+fpaqmySlSINyRxJkEadvzp5K1dHmpKAHMo6/x+K+f8KhTrqAFQTAJFvM+1dS2JsNpTSVNyp1I/eBaj34gCYKY5corG8f7PwlX2rBpSCkypkjhPdHT+n2jSgmA3nSlY4VpWkm2ikkaiioUVA5TerlCCIVYcaeGd3vEbzoWAtPwkGMp6REEHoa1RtNyPCe/dvpnwnAeFRHIK0IVoUnrUu7W0w+grQIzXy6SRMwAY9KDOYpK1qbWnMkm6TyPUcwax3MMvGOLUBlWjQ8eR4ir9KjCpQmcwV7kUF3n2uH1trUktwkpXl0KptP4dT1HlVzdZtDcSoQcxvzn66D2oTvBs5bC8plTaiCF+ysp/mj3v3gVtTGR4WUnhVeOkX+tKJzNqKVC8R76/zTKkKdKQk2mfAgeddfGEwjiBmGU/iTY/70kbxBtt0KbeSsAkBB1TGpPKZt+jQjZ+1yRBUfOaHbRdQhwyYC+NJNgZ+IT2VIjWwPMU29KmBGh9xQ0AByFaiuo7o7WbdGVUSASkG505e1a7vvuNbRcZWsrC+vIEZ0+oBKe80gbEQ9BfZkpRGYoMxNhYG96d91MG74xxLs5lCw1MxA9Ei3tSfw/DLaKgP0XtzMfajPLB3vRnE4NnDN5UJylRzZ9TI/5IjvSNvBiluLK3AQTz5QLa057TxFikgKHf8A3vah7jAAkpEG97z2A616FkFN9ax31g2TpXNNpMBYEG4qjhtiPOGG2yryFh3J/vXXWtmMLIV4QE8jb6mjLmzAMqWEJCCJGX8yaqtYzSLTrpHvzojJWlEGFAaazfq2pf3Y3fVg8PkcV+9cyqNpCRPw9evrVoYUKUpL/EgnhUBaOiuY89KMvY9sZQ7AUbCTBJ6dTpr51ri0uu5fB8LwlDjUoyrsBlEEdz0qoUdFb7nj964xmlG2w4d9QYVLSSUNJSkK5BMZukmL2/OiOAbXkGWQKE4dKsM3L7klHxKA5cpJvedPQUR2biVOJztohJiDlse4kiRVXE2kacas24CYOvD3zmjSU3mtVpVMWy/Oq7DvI1dInnSulPGtPDPI1kE868tRFeQomoqb1z7eh9xxZJIKRZIGgHP/AJoxsLZCm0FwEKUR8IsPU/lUW08LL6kqi6rTpermAxHhAoJJHXpWgtZLQSis1tsB0qV38K0Xjy21CkGZvSHvZgmCo4lQcDhEJCdCoDhzGLaetNONWSkpFuKb/wDFRsxGUwqbQbg0q7h8pU7mIngaYZxP1JTlBA473pR3MxIadStxZQgBWYGRqIkTYAGCT2510RrDMPFLighSvxCxV5lPxWqs7sphH/ZRJEHhEeXSl9WCOHczsKUlAMlrVJ7Jn4fp5UBtlx5sLbURNP4vGMqcyrTEb29/emjenY6FMqXrATmTyI6+YmZrlmKwPhjLlzN8jH17966BsPexLrnhPJyFQhbatD3STqKZcVs5AEKQlSTa4mPzozjSVIDbmo0I93pMpU2vpGyCk9d+fI864WzhYPAdeR/LmPnVoKUBlW1nSb5bKST1jUHuIrom0tx23VFTZFzdMRlH8pSb+tRD9m0XGKKQBJGTNHrmH0pQtvNfoUCPexo3SpdMrTf3uD6Vv+zZxCkrQlsNiIKfzphf2fCwcxEaf8zVHYmwPsolClvZtbBIHsakxOI8W+YyPwkgDt3ohRiVIzNwFc9Oo91BdWzmykGO3zr2J2esgFICjzE+3xVJgMKsJUFzextMW5QagTiWkznW5NoykX96vLwhFw6oDncEj2AvXdJj0iFoR1yR/wCNDQ3h1ElCj4VMMMyqARl8zFeehkJLa5kxBIIiqu0sEXGCptxZ6g6mOkaGlVvGxawuOXTrFz601h23nP1RG4kn0FVfdZaEXzHQwI858KaXsO0pwOKQ34yiMpKQpVhrBkiBFx2ohs4KBuTEXChHtS6xtdXgAic0mTpN7actKL7PxSgMywfKruNqAg9VVaWlRCh1++dW8akKlBQlwAg5VJkek2NeS2oWQcqeQ0ipMU+CBEgkgnSY9ayoAd6AKYKJN6BbZdPh/wAVTXEmFp5GdLcjoe1aYZ0+IkK2gpRI+EIgKmOf5/SruOw6iiEKykkcUTpyvVNvDvSknEExrwC9/laB6TRwQUx18PsaGtJzzHDf/wChWGsWhSI/6gqZzJVkII4SIIJuJUDB6Cs4V5KVFK8askKBKIIIMi06lMjQ9a9hsO+kiMR0n92OLzvRXB4V5Kgov5kieEoEmeqhex+lQuBv7/xrkoJi3v8AzoTht01hzO5iVLGlxJNoBUSddL86ld2K4icqgqNJ1o2Xl/FboB18689i7W1qOnc41cYZsbeJ9aUcfnSIcSpM8yKiwWMQVpzAW5/7c6a8bkeaUlQMW8/SkbF7POZRbzZR+K3PQdaYRkeQUOD7Uo6lbKwpBnzpsYxWdKx4eaNf1rSztfbCm3lNgspACPjJBukHobTUWy9sON5gL5ok8/emDCpSoLW6yFK4YzamSEj61QtpwySY+kcPzVkunEQAb9VCHXQ4hKlttFP7o5VJgSuPvzKTKrQPunrV/wD6+/nSzKLyLoKiAAgyoFQIVK0jLe0qJq6rE4eA4GWiAClJKZMQ4oa8oQbdzUqUsEFIYZsUJIKB951bY8oUJ9TSnzjKtppj5Z4aGKFo3hdTkMtpzpKgchIsAYBziRE3GhTEXqYbzugEFKBZfEQqOC5tN7cpEExe5BBteFUhxzwWYSpOY5R8JI4va/pUQLCpQGm83hpVlyixASr/APtPsaqp9pxSAm1+VxBEcr3nlUhh1IMnar2CeK20OiE50yUzYHoO0zVYIbkpW3Hlz9RQr/qCzCUiAOSRyFFkYVagmUkEix6frpTZRk1P3oAX0lgJqTGY9tsAJbR2sOX9qWcRtdZJvY05jBIKcikzExPfWsYTCISMobAHlVW3UIH6ZorjDi9FQKCbK2ukNEKvIMj5UFcxaVEoDSYPVNvMk6+tOO0tituJsAlQ0IEe8a0qL2WtK8hSSdJ5Hnr0ozK21EnQ0s8l0ADUdX8xXsOm8ruZiBTE1hFrETlgixqvg93uIKWZKYgA2HrrTChuKE88P20dhkic1QOYaQO1a4lomISDA8qtxWaWzU1loLtAFLRjNqPgEq1GgNjQsoWAoBWJkRfwk3vFpF9Z68NHMXiMqSQkqjknWqadqm/7l2x6C/cX0j60ZBMWHlVHAmbnz9KoyvLM4gEkf9pMi0REaGZntWzLbqwf3zw6ZkJQdZkW7R61dd2meTK/iI5aDRWuh/KtXtoKQJLLhkC4g8gYMkRr8jVpVw8qrCeJ/wC6reykLCSC4VnqoCRbThit3EFDalQMwBN9IFz8qr4LagIkNrE8iBP1q64+FoXKTASZB52NrGl3c19qInLaL1G1jUhUAACEnvKhIH+UE0M2jhGVrzpcAVE3nTLNptoZrVn7PkbkqBcDKiMypTnCgiCBebpi1vSq60KzKaUloFKm0fEuCVIypAhP4bXpROIdQZEePMxpRVMoVY+4rdjZrCEKdBC4ItcCTy0oo0+2hMLQkJICkxxA3tEgXBg0I2ftBrKppxeYOqzcJWSBw5bqTN8pVczA71aONweXN4jipI4v3hWIGYRaQmDM6XqFYpxz6isdU+nVFQnDpRYJPdV/Dqw61ZfDbzZSBwgykySNNLmR3NWUMNCUhtFogBI5HMOXJRJHQmgjGMaafSAFIa8IL8RSwEkKMJJBVpeLiZ5Rerbe02c1nkiFZbnU9vpRGlJI+sialSY0mKycegEtlgCQlBTbT7qSI0gmB3rVeMHiFISE8SgDlTqBfvJAv2IoHthak4pwJWfEzN+GIuSqLdgkc/KtEPEP3VI8Z9Md0gSfWR7Ukp9cweMbcYo3RCJ5ek0w4fENkhSGuPKVcOp4sp/vWw24AAfDMGYM2Max5UH3YxOZaB/8so//AJDQ95xS8PhQnVQxB/ymfyqy8S4UhU7cuI+9cGEhRT73+1Mzu2TNknROsQM1wZongcV4gJiIUUn0pJf2kFlSwIB+z2GgtBA8iKZd1XczbvZ5wfSrsPKUuJkXqjjQSmaI48nISFBEQSoxAAMmSbCwN6DuYhSmypOJRCZStVhCuGOVifw/ziOUkdt+EWXA6vI3lOZUxlAvI76cjSQXkuvB9LyR4uJaKwgpV4SUoWhorN0BalCYMwSnpWqy3mBPDl798ZpB1ZSY48/fvhFNbLivDDqcQjwhmJXaABGqv5SFgm3e4mozj8jfiLxjQSowlRKUpkBQIBNpmLfyEc6B4raGfBZFKRmS6FuBICczScTlLpSLZVFMk6fEdK8H0hWKX44aAXiFolIUFAeGlRGcFJGdJBAucxiKv0R34+9ufDShl3hw49fPaKYmH15xmfSQcigkC+VRMGMuhsJ7a3o1SiVrccw7rqMqyhhShpClFUiCoadwojMbDWm2grEUdozP5pexAUUkJKQbfEnMnW9pE2nnWi8MOSW45AoqwKyjSizUxUDmDUpHAGgQbS3aLz871bY2cAiFBClcyEAD0FyB6mt0VNhtT5UNSjRQga0M+yjXw0W5ZQbdqsNoJbcDYQlahbMITfWct9JrbEK+orPI+Vcq4ipgTQxnd10qZLimgGQ2JRnzKDd0ggnLrzial2nsdxbji0rbyrKDlXmBBSmAoKQQQRew61cYfVm1N63xXPzpb5ZGnvSPKr9IqZoIdguohUYcZQALvRYEC2aJAOtQP7LUoRmw4sACC6CAEhA+9+EDXWj7Lhym9DcS0M8x0q6cG2bGqKfWmsOYBSlNKbWyS200gheb4kKCgoZYtIGtRp2I5J/8rckm7upM/i61u1/E9avO/DVlYRE1yXlRVPCbLeQ4p5P2UuKJJUfEJE6xJt0tWUbHdCw5/wCGzBal6ufEqMxjNF4FqsJqcVX5RAtXdOqaobO2KtlZW2MOlRBBguEQYMQVdhUrGxSkNCGYaz5LuWz/ABam896thVZWq1V+VbFgPfsDuqUvKVc1Ra3XZsSlNoiFr5eZrG2ME600tOFEJVmUsAqKyoqRdMcWgPaJq2lZ61sys9a75NGUhNuqoOIUFXv11YezFGVRR4hSRIHBmi2s2mNZoaph7PlShsoUTIibX1EAEaVfXyqRJuKZH0ihEZ9ajabfBTKWSkwkwLhPMchETbvzrzjT+UJCWZBGUwcqYA0B0vpE26UUY0rNCzX0rsm0mhbjLpUhSg0CDxGJMTaCRI9xc1Y8Yi1TZtarLqdaKhMV/9k=",
    },
    {
      title: "Transformers: Quái thú trỗi dậy",
      img: "https://upload.wikimedia.org/wikipedia/vi/1/1c/Transformers-_Rise_of_the_Beasts.jpg",
    },
    {
      title: "THE NUN 2",
      img: "https://mcdn.coolmate.me/image/September2021/phim-ma-my-3.jpg",
    },
    {
      title: "NHẬP HỒN",
      img: "https://tintuc-divineshop.cdn.vccloud.vn/wp-content/uploads/2022/08/nhap-hon-phim-kinh-di-sieu-nhien-kieu-cu-va-nham-chan_62f471fe2082f.jpeg",
    },
    {
      title: "DC Liên Minh Siêu Thú",
      img: "https://cdn.dep365.com/wp-content/uploads/2022/12/phim-hoat-hinh-chieu-rap-4.jpeg?strip=all&lossy=1&quality=90&webp=90&avif=60&w=800&ssl=1",
    },
    {
      title: "Liên Minh Thú Cưng",
      img: "https://ramestar.vn/media/uploaded/2020/01/05/lien%20minh%20thu%20cung.jpeg",
    },
    {
      title: "Klaus (2019)",
      img: "https://static.wixstatic.com/media/5b44bf_d3537ca79c6745f0a884b53ca68ad0bf~mv2.png/v1/fill/w_824,h_590,al_c,q_90,usm_0.66_1.00_0.01,enc_auto/5b44bf_d3537ca79c6745f0a884b53ca68ad0bf~mv2.png",
    },
  ];

  const handleChange = (value) => {
    setMinValue((value - 1) * numEachPage);
    setMaxValue(value * numEachPage);
  };
  return (
    <div className="background_list_movie">
      <div className="container lg:flex container_main w-full mb-20">
        <div className="content_left w-2/3 mr-10">
          <div className=" text-white mt-28 flex items-center justify-between">
            <h2 className="text-2xl font-extrabold">IN GENERES</h2>
            <p>View all</p>
          </div>
          <div className="mt-10 ">
            <div className="menu_content">
              <ul className="menu_item flex gap-6  font-semibold text-xs text-white ">
                <li className="text_viewall ">
                  <a href="">#ALL</a>
                </li>
                <li className="text_viewall">
                  <a href="">#ACTION</a>
                </li>
                <li className="text_viewall">
                  <a href="">#ACTION-ADVENTURE</a>
                </li>
                <li className="text_viewall">
                  <a href="">#ADVENTURE</a>
                </li>
                <li className="text_viewall">
                  <a href="">#ANIMATION</a>
                </li>
                <li className="text_viewall">
                  <a href="">#HISTORY</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="mt-8">
            <div className="">
              <div className="boxx__header">
                <div className="grid grid-cols-2 lg:grid-cols-4 ">
                  {data &&
                    data.length > 0 &&
                    data.slice(minValue, maxValue).map((val, index) => (
                      <Card
                        key={index}
                        className="shadow-xl"
                        hoverable
                        style={{
                          width: 200,
                          marginTop: "100px",
                        }}
                        cover={
                          <img
                            className="h-60 object-cover"
                            alt="example"
                            src={val.img}
                          />
                        }
                      >
                        <Meta title={val.title} />
                      </Card>
                    ))}
                </div>

                <Pagination
                  className="text-slate-700 text-left w-full mt-10 h-10"
                  defaultCurrent={1}
                  defaultPageSize={numEachPage} //default size of page
                  onChange={handleChange}
                  total={data.length} //total number of card data available
                />
              </div>
            </div>
          </div>
          <div className="empty_space h-16 mt-28"></div>
          <div className="">
            <h2 className="text-2xl text-left ">IN THEATER</h2>
          </div>
          <div className="empty_space h-8"></div>
          <div className="video_wrapper">
            <div className="object-cover m-auto">
              <iframe
                className="iframe_video w-full"
                src="https://www.youtube.com/embed/sgnO5fO46pE"
                title="YouTube video player"
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                allowFullScreen
              ></iframe>
            </div>
          </div>
        </div>

        <div className="content_right w-full mt-28 px-4 sm:m-auto sm:w-1/3">
          <div className="text-white mb-20">{/* <a></a> */}</div>
          <div className="text-left">
            <h1 className="text-white font-semibold text-lg">
              SPOTLIGHT CELEBRITIES
            </h1>
          </div>
          <div className="bg-slate-600">
            <Divider />
          </div>
          <div className="item_performer flex items-center mb-5">
            <a href="#">
              <img
                className="w-28 h-32 object-cover rounded-lg"
                src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJAAegMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAADAQIEBQYHAAj/xAA5EAABAwMDAgQDBgQGAwAAAAABAAIDBBEhBRIxQVEGE2FxIjKBFEKRobHRByPB8DNygpLC8RU0Uv/EABkBAAIDAQAAAAAAAAAAAAAAAAECAAMEBf/EACARAAICAwEAAgMAAAAAAAAAAAABAhEDEiExBCITQWH/2gAMAwEAAhEDEQA/AOMNRInlpwmNCKxgukk+BRK887CEkE72G4NklhtQwM4VadPgSRJUySG5UWa5ddOLtpyvbg7lWubroqVeAgbL18p+0ErxbZBMNHt2EN4cQTY2UuJ8bW5GUaWaN0RG3KGwygVbVaUFtmVWfewrOjbZgchPwkfQOosDTdV7Wjqp1fJvcAohFgjDwEvRY3hhsUXzm9lGcElvVM1ZEwwCVp+JDBR4WXKDAG4CCSbqQ9psLdUPyzZLQQRXhhec0hITb9kaAmKDYp5Fwhnjd0CkwbHNGcnhSggbFKGu7I9mA8hE3sATUSyuIIep8BIjsOyizWdLhSqc9D2QfpCHMLylODBZOqAPNThE4tuFEAC+MWQtoR5GPAQPiTEEa3KkMaQL9E1rRa6fc2wkbIOEx6p8k4a0F3BUa9jc2AQzIHHjPIv0USCFlmaNtzYIcjhZrgkfuc4saRjpblNMtjbY2zeOov3TgH7g1gN3G44HCQSfIGvIsOCnxxyTP/lx7iejQknpJTKXCJwJyWkEIWhqYvm2a69iQfyRYXNe0O7qG5kjXbZGkOtgHCLE7y2Bp6coikg7d2AnxOtLZDAG4HOeEoNpwll6FeC1N/MBUiOUbAE2UA2KjOu04UhKmRq0HneCy1woe1Ec8nlMTN2KlR4mwS3dbhM6qzha10YuENQlTMfh+IYKCXG+bY4Uuua1rnAcIFLD9pqBHe25H9EXT1MJZZwIW7nuNgFp9H8HVlS8CYeVuyXYwrTQdMgpXCRkYLu55W1oLFgLCHBY8ud39To4fiqrkB0vw/R0MTGxwtLmj5iLk+6ly0dNu3Ogj3d9oU5pt0TJmm3Cz7tmrVeGJ8Y+HoZ4TV0rA2VoyBi4XNpWubUPY8XPZy7bUs3sfG8YcLZXI/ElHJQ6tI14FzltuoK14Ml8Zh+Vir7IjUp3MF+UR1g+6HRxndut6BFlbYm6tb6ZEuBHP+EIDrlOthOaLBAgEhJtRC03SbXJ0KCDHbhhWlO2zRdQt4JAAU+P5QUxGVdcJHudIQ0ZsADwl0pw+3Ndx6dlLrBcnAUWjic2o6ZacjNj0SvwaPqNzQVdLSxg1dSyP3K0OmazQykMil39bhc+o2wMldGykjqJgCXOlfgfr+AVs6mMVOyohbHBIRu8uMWAzxzz1useSJ1Mc2b6at2wukijLiBcZ5WeqPEGqSTFhfR0cHHmSHJVro8n2nSYjIL3wfdR67QWTyl0ULHNe4G5+ZpHryPoqV9X0ua2XBKaqkmFm6hDUi9ztscdlR+MdEfqEcDqSMOqRIGt9jfH6LVU2gQQuEjoo9waGjaOgTp47zRxxEMduAa7m30RjJqVoEoKUNWYCo8JO06kbIZ3yT7SXWA8sWyWjrewOfRZ2sFpLLquuGVtDM+qDd7A4Ajgkjb/AMly3UrCosOhWjFNz6zF8rFDHWoFpwiAYQo1IbayuMgzjovbvRPe1M2nsj0nAMbPiGFZxgBoVXHIS4C6smNJaFYVjJow8r0UYika7vglAnmMTrJGVmRu4SNFkXTNjp9JTS2kc27h2UnVowykJjZgcKn0SuAG2+Dwrmskjlpg17yzN7hZJRqXTqwlF47LnwvJF/4gB8jW7Rc7jm6uKGeOeIuhmZK0HJY64XP9KjlZPM+lhmkafuOfdjie9+n1Wu0+pnhgBkpmi+T5VsAegSzVjQfC6e8tYRyVSV9S2Cbz5LhkRDnW7A3VgysZNGXsuLYcDghVGsQvqKSaKnYXzSgtYwcuceAqo9aGlyLZU+J9dg1Mxw0pPltO5ziLXPQW/FYLUv8A2He6mB0rJ3xzMfHIxxa5jhYtI5BUKtN5St0IqLpHKyZHPrBxFSWEFRmj4V5riE5UTGkE5RLM7KAZHDhJ5zkUAHT/AOK1XbXNDQqmKIg3UuN5HzHAQ3DoyJWOvKgo0wvJcJNmE2wKJemSkfBex5BVwyofIWl5+EDAPF/VQPDukVOpVjfJjJhjcDM8mwA6i/f0VrqNCdNkIdcxX57e6E8bcdi3HlSepZ6czz2NdLWsYGt+Vot+qt4vsDPhinkmPVxkOFkqaGCZ22SQbHjm/C1tNVaXR07YwxjdjQARbj1WGd/o6sMnCSJQYzsBDT3TJmGpZIQS1jWHI72UGq1aOpqIYKEB7X8lv3R3wtTpcMRq6emdlrWmR3rbj8/0QhFpoWc00zB/xGpRBrlJOWbZamijkmuLEvF2k++AsVU5cSu5eL/C8XiSNrxKYqyBjvKeT8Jv0d6Y+i4tqtBV6bUOgr6aWCQGw3sIDvUE8j1C3uLTOS2iG02akOU2+ErT3RSFsaSvLziLpbhMSyxiDS3okc1u05CHFE4Ybck4C1+geAK7UQ2fUpTRwHOy15HD26fX8FnhDZ8L3Kl0yFNSS1VQ2CmifNM42bHGLuK6D4f/AIbb9kuuylvX7NC7P+p39B+K2mk6Np+iU5ZptKxjjjzOXvPqTlWkbdotyTknuVo/HXpWnZTDSKWgp3Q0EDYYwcRsbg+vus7rOmsq2PimYbEYPUFbCrq4aaZjZX2L7u9h3Polnp46qJwba5yCrvy8plTxK7Rx8+FdTa4/ZDI5l+XtP6hSqfwXqtTslqqxkTXPALG346m66lHT1UH+G0OHVpRIHGWuDXRhobES9p4JJH7LI+t0i7qXpn9D0PTdMY+DzZGyA4lcL7vfCtaCkp4a91RDPLLI5m2xADWjP7qWynD2glgJI5UmmpfKcXvsOwASxgrsdzlrVho2loJJUeuo6WupzT1tNFPC7lkrA4H6Fera+OBwbYuN/iA+6PVGbIyRm5jtw9FoRUc/17+GFBUb5dGqHUkp4hk+OI/1H5rnGveH9W0GS2pUjmMJs2ZnxRu9nfvYr6GKHNFHPE+KeNkkbxZzHi4I9k1Ao+ZiSvbl1PxV/DKOTfVeHXNikOTSPPwu/wAp6exx7Lnb9C1hj3MdpGoXabG1M8/mAlpinZvD3hWg0ZrHhvn1dszyDI/yjotE0dSmUhEse8eyWZ1pAwfeH4K2kvqif0WK8shcR8LcN9VH1HUm0odHFZ0o5J4Z7oOq6iKGFsMB/mv+U3+UcX/FZ4VBcdzRfdkHv1HP98hVydsN0HkL5JfMeXF4zuP1/ZSoJJIx/LeQBgC+B2x+X0Kg+cNl22AHB5FrY/Ij6E9keBxa2/IyCCeenfsQP9QPRIyIsYtWnjuHhrm//QPS18g+mf7Kn0NU2sBeI9rwW3cQM4OOTwqQvc42sSexPJv/AFN/9vqrXRbESm4Py5HXnPp3SJD2EGrQXtGx98AA2F+2b9bIL9UlmaNgEbXDkE3scjPTH95VSSLHcN2SOMW5PTra/wBAOqex13ZFiHEnHOb349ne+OiNAbJDsG5cPUn3/wCj9QOhTGVLoXNMDnb/AO/+v9yE4Fxtax6tHTnHS9iSPdwPReewMABAdf8AA4+uOfpuTKxTR0lQyqgEjeeHDslcQFnYKx9LKHx3N+bg/EOc/mfwHUK7pqqKpZvZh33gehViJZI6DslTWuFvZe3IhP/Z"
                alt=""
              />
            </a>
            <a className="text-white font-extrabold  ml-4" href="#">
              <p className="text_name text-xs">Anthony Ramos</p>
            </a>
          </div>
          <div className="item_performer flex items-center mb-5">
            <a href="#">
              <img
                className="w-28 h-32 object-cover rounded-lg"
                src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAIUAYwMBEQACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAFAQIDBAYHAAj/xAA6EAACAQMCBAMHAgMIAwEAAAABAgMABBEFEgYhMUETUWEUIiMycYGRB6GCscEVM1Ji0eHw8UJyohb/xAAaAQACAwEBAAAAAAAAAAAAAAAEBQABAwIG/8QAMhEAAgICAQIEBQIEBwAAAAAAAAECAwQRIRIxBRNBURQiYXGhgdEjMpGxFTNCUsHh8P/aAAwDAQACEQMRAD8AyO3ANemERv2lk0rhi1PhxSOFT3ZFyFOAc4+gz9edKFFW5D/UZN+VSv0MQBk86cisdtqEPbedU+x1F8o1djwzZ6lYwz21xtkJxMCowp74x+aS/FWxetnpLqKIzcZVr9OBJ+DG2MbW6SZlbBXG3H3z61os+xdwb4TFfdOP67/uCrvh65gvFtEImlZN48L3uXMenka1jncbaJ/hdUo9UZtfdAqaAx4yc/aiqrlZvgDy8GWKk297ISK2AR8NvLcSBIY2diQOQ8zgfvXMpKPc7jFvsQyIyMyupVlOCD1BqLnlE7EeKhCyF59K6Mdhe91qe+tWhuEG9pjIXU4HybQMelYV40YS2v8A3JvO9zjpg1RRIOSYqFlqwjXxC7KG28wD0zS7xDJdUVGL5YbhUeZLql2Ro7Vtd8JZraCLbnkjOASPI4HlXnd872PfKkxsevPptm1tLZmKaR3ch5M4O7B6fnr3pnRVO+PVEznbTGaV0tP7DrHiHTLNS6wymbYEBJXAUdhz5DOTWksW72KldVPjzVoBcST2d1fGewz4cnvMCMYbvRmFCcd9S0B+ITUqoLqT1vs9+wHIo8Uh61Y2XCM7Mqv7XPhVbsMY3flTQM115KS9EFQfTQ37mdbJJJ6mjAcbioWWgtdGI7bVkFAqFD1FWWWbeB5HTsrqdvLqQQa874pL+Pr20PPD635SfvsP6brN38WJrFikaMeXIkgZxStobr6gbU75r/ZLJAYWJJAJP/O1O/CG9zX2/wCRL4quYsHkU6FAwioQYRVEHvcTPbrA8jGJTlVJziueiPV1ep31PWvQrkVZQmKrRZOZAOXeujLQokPcCrIL4gPNelQhIjA1ZC1YSymYRn+5VhID5HpSXxaEVKMvVjnwucnGUPRBy4vZsrvuUg2DKEk+95jG086RoeJN+gF1W5e51D4hBeONUYDs3UivReEw6aNv1Z57xSalfpeiKpGRTMWjCKhCNqhBpFUWMNQsbiqIbD/8Z8cD2lTHtyW2nOfLGaT/ABtnuO/JxNf5fP3ZIeD4mikMN6HZMjGzluHYnNX8baV8PirvX+WULnh6K0tWZZ/GuFdEaJFPJm7deZ9K6WXa33NK8PFnLThpfdmj0H9P1uEjuNTZ1U8zChwfoT/pVSzbF2ZjZjY3aEPyw9f8IWihJNMgSNkXa0XZxjrz7/zpZkddsupvkJx5xqXTrgzceh6kuoR24SWK3Z/eLQ5CjzDHlWEIblpoKlaoxbiwFxRoEfC0VxcTXJvN5+AD7pMh/wAfoOuR19Kdyz+mpKC0xHHE6rG5vaM1pnC2pa/GLy4vQkeMorAnPYHA6UotzJyfLbG9WFFLa0iKEXOl3o06+wc/3UinKv8AQ0y8Oy25Ktvh/gXZ2Kkutd1+TY6FoFpqtkZXujHJG3xFx0Xz/nRGRlWwscV2NK8WlVQlKG9r3fcsXHDGnxWyXCXUzxumUATJY4yPt/tWPxlxpHFx3Lpdf5f7gnWtCk0u1gmlA+IvvYPyN5fj+tbUZM7LOls5yMfFVM5QjprtywFTMSGiteJp7ewa13hjjEcpPvIP6+nlSf4Wxvej0c78Jz6utD7DilNP0yS2WMs5LMshPyk+mKt4dje9GV2XiTnvr/D/AGNfwLam9s11SeBY2mcrAAMAj5S2PLkcffzoe1OuXSSVsJxXlvg3aECM46LyFDs4FQ8zVEGzIChO0bu3KoWcg/W5Wit9IcfK8jq327/uK5kdw7gzhJJn0hfabpPY7iPYqgk4Pbl+RigbNb4GVe3H6FTj6yh0+x05oZJJZYpWy79efvY+gxWlE2p7RlkV7gkxsF9LHF8E7VdNpwfmU9jXq3jQsfW33E0PE7KoKvpXykv9tagPD23DDwlCp/lArn4Gr1OH4pa9/Kufp/2V7rU7y6jMdxcPIhbdtZiRnzrSvFrrl1IyszrLIuDS0/ZFOiAQiZgoyahmP060l1bU7axhOHnkCA/4R3P2GT9q4smoRcn6HVcXOSivU+hNPtYbVYLeFQsNtEqRgdgBgfsK85KTk236jxJJaRZztR//AGNcMskB2qxH1/aoWinczMUYbjnqMH1q4x3yRtJ6MB+pNhHf6EPaXbkzqh5nafmz/wDA/erm12Kh7nKeEtakhhe0nSaS3Q7/AILe8AfTuOdB3R/1IPx7GvlCHGE9xeW9rLHA0FmmQsR5kEjkzY+/5rOppNo0vTaTG6ZL4tjHn5k9w/bp+1er8Pt8yhL1XB5rNr6LX9eSwaNBRhqiDahCnI+WPpyqGbCPCd77DxNps2M5nEZ/i93+tD5S3SzfFerUfQ1mRJEz+Z5fQUgY4IriURLIrEe+w25OOZrlnSJpGHhuVIJAxVeha7lWGL4Rdv8Ay8646mkduKcjnf63gR6DpsAk2LJekvhsbgI25fkio5bRcYrZhuD9IVhPcZADM0XMY5A86Cum29B1EEls189jb+AUMQkBXqWOKwCAbZcMWctvNNaSCByfl3ZB/h7Uyws6WNLb5XqA5eFG+OlwyDULCDTdMt5ZWWS5uUbMTZzHz6jHkQevXPSvS03u+T6P5V6+5562nyY6l/MAy9Fgwf099KWziF1p0ksuPecSYB+2aTXTt8x6Z6ijDTqi0l2XovYx5NODyZoOAbe3ueKLf2pN6xKZUTGcuMY5d8dftQmbNxqegrDipWcncdJu1kDwo2XUZ2sMHr5UjY2LF4obbnHMg+eCK5ZaIoI8T7FjKofPqfWqL2S7fkT+I1Ui4nKP1vcvJosDDkqzzEfdR/rXEvY1gu5Bwvp6R6I13PcxIixhowWGMnmSckA9+/Ks4UKUtyNZ3uMdREl4h02e1mSCeFxAQu2GUykr2JYgAntyJ6VllVdLTj2NMSxuLUgFpmrkanIvuoc5CB88j5+tYuLUdhMZbei5xFmQwzgkpjb16d/6mn/gd21Kp/dCXxmnTjavswJ1NPxISbj/AIiPvXPTH2O/Nn/uf9QMJD3rnZl0mm/TtoG4phMu0yLGxgUpuy//AFuoTNf8MKw0us7grBow6Rws6jKqG2kfRudJGNET6fdG7ctJaSRunLxGwQfof9qojLxx1OPrXRQ2NRzbGM9vKqIco/XbT8ppWoBiBiS3b9mH8mrOz0NqvVGL1Kz/ALJ4RsLGeVZfbpPaEX3jtjAzz7dWH4rSv3NKnBT3PsB9MmEcc08W0xxDb9WParujGcdMJd8dfIetlCnxlOXByW8z3qsXHV6lD6cfcCuvdLjP68mrR0vtKIQ7vdyPQ0Hh2vHyYt++mH5Vavx5Je3AEXHXtXtjx/psQuoOCaogFzWRaQ3fLE6zWzsk0bB0YdiKysj1R0a1S6JbO18M/qDpEHDtjJdyPLftEgnjhiOfE6HJ6dR2JpHbHpk0NY8rZs9BvhqVpHcrGYg67gmc4BNZojCLfEbaOg6nzqyEtWQw/wCsVql1wcwJAkS5iKZ7kttIH2Y1xNbRpW9M5Fx40uo67ZWdhZTiHT7JIuUbNluZY5wc8to+1dw0lyzuqt2SItQ0uWDQV2ZVYuZUDm79T+AaynYurpN3DcXJC6DpE0+mRXkksWycsI0BO44ODyHTv+K1hk/CbmvUy8iORqMt8B+xt44VFpbsiKp3TzOcKvr/ALUvSsybHN+odqNMFCCB+v8AskF4VsJllgxuX09PtXqsHq6W5CPxOW+hNcrYEMxJo0V6KBNY7IKOlWUEOGpmS9lgAYnBddvXp/rSfNhqWxriy3HR3rgC5S50OOFD8S2zEx8xncD+DQa7G8lpmmZkhTMjKi+bHFdHJnNf494Z0FH9s1WCSZRyt7Y+LIx8sL0++KhDkfEn6iXPEl1p4uLVININ14hiQ5lwrFRuOcd92MVTR0mHLfT7xLR772yf2Bmkk8WZysYwcBRg8iQB19awvrcvsEY9qW99yjc3Wj6hC6R3sAlj94gkcyVwevmKESsrfYP6oSXcC3F/FZwi30tRHGB8+OZ/550yxfDbL3128L8v9hfk+IV0roq5f4BW6R+ZY16GrHqqWoREtmVdZ/NJjZM7QvatmYIZt9ahBbHSby/ybaIuoIyQQMfmgbMiMHoNqwnOKnKSSYXteGXKK15MsG5tgU8znOBQ8s2T4igiOHRF+svwhx0FLXUbM27SszuEP+XnzPL/ALoWyydi5Nl0RXCSQS40fVdB0q3udH1O5tSW8O58GXYXyOXry/r6ULDfOzS1RSWjnE8013lrqaWdm5kyyFyT9zWhiV0G0YxgA9qhCwjHwVDc03ef1J/nVkOm8Q63FpfBulR6csa3mqWpNywbqi7kAAP0Iz1OKmyktMyOkNo93o19FNDN/bG34chf4eAewz1wO46nr5dFplfSxK8JEvPYxUH0FNsFylXp+gBmRUbAm0DrB42z4e7bu9cZ/kaN2t6BdPWyqoDzqrHClsEjsK4sk4xbNqK1ZZGD9TUDRNDVRu1LJIB6gdedKvir2OFhwS15f9/3M1pWoz2K74JAM/Mp6N9q6vh1TfBeL5bxY+Y/f+5PqOvyXVzDKoCiEhlTqNw71UMWWuTJ5ePUml8zKialLeatbyXTZXxFyOg5HNS7HjCtv1M6srrmoRjpG+1t7S+4avork+54LMD/AISoyCPXIFJ1wxnOO4vZx5DkD1okXkLOOZzjJP8AOoQICwupcQohyiBiSMADGcg96t9iaD98IRbaJb3gLgWboSpyVbxJD0/i/wCcqi0TQR4T4bsI5I7/AFKV2jb3YEXI3HPU47VH30QgnsPB1t7KAgp4oCntt8/x1+9OcVqNLkAZDc7Uv0JuJboXGsT+GqBIvhKEXHJeVa4sXGpb9eTO+SlN6AoY78r1zyrVx6uGcwm62pLhofmQ8ySfqatVxXoR5E29uTBe4g0PvRWtjt2a62c6K88zQ++mNynIyKGyH8jQRRxJNEn9u77GW3dJcvGyfPkc1xmk3Rzsbu7cdMGq5wOtdmA+3upII2SM43n3j3xmoQ0ut3Dey6eI+UT2kZOD72SM4P2I/Nczi9bNY/Lra7mhsrGy1Th1o75W3QSBldThl5dvQ1lC35dmk6X1aK0+qPFeRxhgIYRlVAHLHQUXhVyujKb90kY5bhS1H6bZSvr2XUb2a8kCq0h+VRgKAMAU/pq6IKKEllu5bZCi5OWNaScILcmSqu256rjsgcbSTjl6UDZmpcQQ2p8Hm+bnr7CeIR1X9xQrybXzsZx8PxktdILbI60YzzSPITmrRGiO+gl9kecriMYG49yT2oXJmknEIog+4KWlwWSr8tUWPk8D2SHwwTMd/idfPl+1QoN3IaS2gD4yoXOO5wBmmN+NGNO/UxjmTsmovsuwWttRkt4fD2KR0yTSV4yfZjKOY13RXdvFkZjzY0/xJVY2PGLfPf8AqLLsfIzL3KMdL69hMsvIjFc2Z03xDgPx/B6oc2/M/wAHgQcnqRQkpOT3JjaEIwXTBaImk59xXDZ1ojLc+a5quonBRlPIU2keMihsfU1US2EdbQLwrZnJJeXP060uyOZsNq4gkZU9aGNSVclcZqEH2SBrgA9B71EY0VKzn0M7m1DgPv71sw6DHam2S9UMAxq+u7v2FtpS8YJHbNIhmWkAVzjzrVdh1Q91xf0I5JDub61NmwxGJbNctnSFyfF2DHPv5VO5zvZPsjHLZn1zV6LP/9k="
                alt=""
              />
            </a>
            <a className="text-white font-extrabold ml-4" href="#">
              <p className="text_name text-xs">Pete Davidson</p>
            </a>
          </div>
          <div className="item_performer flex items-center mb-5">
            <a href="#">
              <img
                className="w-28 h-32 object-cover rounded-lg"
                src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJAAfwMBIgACEQEDEQH/xAAbAAADAAMBAQAAAAAAAAAAAAAEBQYCAwcBAP/EAD0QAAIBAwIDBgIJAgMJAAAAAAECAwAEERIhBQYxEyJBUWFxgaEUFSMyQlKRscFi8DNDcgcWJCU0c5Ki8f/EABkBAAMBAQEAAAAAAAAAAAAAAAECAwQABf/EACMRAAMAAgEEAgMBAAAAAAAAAAABAgMRBBIhMUEiYRMycSP/2gAMAwEAAhEDEQA/AOe8oox40oRdTGF8Dz6VbcOaXEinSp8AKjOVGKcegkXrGjMB51d2Tdt2kuhFbrtQChFxkP8AV0zEkkHyqW0/YSe4qu48x+rZcnfNSoH2EvuKKOZpT7gr7R5VnHjSAdq8d1SWMMMqSNeDjA8TRAeLEzDuqW9hnFeXNpcQwiVosK3mR+1MoZrEzPCZJCDtoDFVHsVP7/Onlny7Hd2ubWeQAZGmZtQ38j4/Go1mUeS046v9SFtiJrhInZVV2ChvAZoq/wCHzWb4chxn7y+HofL+fDNOLzk6+hkcxqHQDOcaifhU5JbTRB1aKWNoWAbVtkNnH7GqK5rvLFqan9keFc1gyHNEKjp3ZR3xsa+K5O1UFA2StZUii3StbJQO2aAazHSvimKxxRAVHKij66DHOFhfOPhXQOHLGYJWRMY8zUNycFPGGV8hTbsCwHTpV/aLHHbS9mSR45qZxO8fdjw2ToBnoBUtpH0WQ/1Db4VVcxOPq11CKN6lv8iUeooo5gse4A8aH4sEgCRyZ1sAzefjgUQjvGcx4GTgMfw+tZR8FuOKx3F9F3o4VU6MEsVAGQPYUtUl5GiHXg2cm8D+tLxZXLdjGwbH5q67ZWsUcZUjoNgBsKmuTLOG04YkugLLIO/gnFM/94LaKUwQXlu0+ojs2/brXlZ7rLkaXhHrYIWLGn7YykiGDtUFzvbmFjNFEpWUaH9xkg/vVnPfSC3V2QdpISACcAfGobnHiyS2XZJdxzydquViUaQM42PjVeGvkJy6XRolrfJd1ncanyRnzznHzrYUw3pQJmwwDjSwODnaqCCOO5gR0xpxXp7PL0KXXNYmPApjLaFTQ5TfBFdsAC6bVpK0fIlaTH3fWuOKTk7vcXcdMQNk+XSrqDSLaXDZHmKg+TWf60mCrnVbsD6birm2GLRwT570pyEXMRT6tfunOrqTUwP8CX3Aqm5j7P6tbvHOdsCpkf4En+oUUcwYfaywwoO/IGQH1xtT/k1puHcU+hXmBb3KbKT1Of7HxpFDgCVz/lrqHvkULBf3Ud4kqyFZIz2qE76dupqWWepOS+KunVI62luLeRokAAz0HQVult7QBHmUF2OMKNyam+VeNG+e7NzMZJNYbvbd0gDp/fWnsQ0XbXUkoVNgraNfZ7bkDIrxnNRbTPailkhaCuImOO0EpUMkJDEY8PH5VF8/x2cVlaXNsY951YY/EME+FPeZx9jmK7tpsJrYR6lx7ioTic0d0YbCKNp5YYwFK7oGO5PwzpFb+Nja1T+zHymtNaE9tbpeXqRamAdsBvennCeziD2ykiRSdiMbUBY8PkEva6nDowI0R6gT6/8A2joiZuJJIqFHJYupzW1M89zoYaNQwRQ89krnKgZo5a+wM0RdCC4tHj/CaEKkVVPGGGGHWgrnhyscrRF0e8mxs11eKo73YbfrVnZIVs5FbYnpUZyeoN/ctqOsQ4GPU10Ox4S7Wy9q4TtDtnwFA5Efx1He2aMBSqjJbOKnT/07n+oftVZzZaPaRlH72n8XnUnJtat/3B+1GQMCdyiSEY6eNDXLW7hGh7VHU7+Ix6GjYLdpJEywCMCcdc486XvFJPfvb21uzSM5CxRAnJ9KGTWysLsFcu8Qew4t2yJ2iKjBl6ZBxt/flV1w3my27VY7yJo45DpBHeAqbk5Vv+GWhvLhEw+8iIdRiHr8+lAOhLLgFgN222Hh/NZbjHm7lpyZML0P+bOIcOFzqsZFmjKgNGJNic5BGOuKnIr2FDpitwCTgP2jb+mfCts3D7kyRu8BUTLqjb8y0PbWrNNLEi95AcY9qpjlTOkdkurrqYc7q8EdxEGjOrGpfvZ8j4Gt8M5+thESWOk97G5x1HzrDlR1m4xDZ3K6oZH3U9NXh866Zd8Mikiw6Kdvy7ikvMsddLQ8YHknaZF4GeuK2KM9PCs7u1a1uGjckjqrHxFYL1rQntGa009HuM4r1gNvOvdgAKywMUSbFvKDmK4u5F+8I1wfjXRODSycRKtcyY0HbFQfJOEnu5GUFAqjHxrpMNraKYpUkCx9Wwa4BKc+yOZJAyaRsq+3nUjaWFxxEdhbKCxkBYnoox1NUvPd4txITESEDaR60VyRAkVg05AzK5yT5DapZsv44dIvgxfktSwW05NtIVRriaWVvx47qn2p3aWnC7HItbdEcnvOowT8fGjnxMWBcJCnU5x86WXXFOBW/wB/iMGR4Jlz/wCoNeXV5cvk9VTix/Qw1RHuopx7dahuN38cd9d8P4TbxqJ2CMVJ3c7Hbp1/Tem9zzZw2MaLcXEurqwXTpHid981OR3/AA+143He29q7wxrkRsxGXx97fP6edW4+Kp22jPyMsVqUU9/af8qWBUH/AAsQCHy0jrUIi3Fvf9ukZKSSEZxs3nT3inNdzdWxtra2WBZNnfXqPw2GK1y38EENnw6SAo+ldEokBDHO7frVcMZIXf2DNeO38WBcMuILXmCGbsn7GKUFsLuvkMePhXXEKzxBwcqwBWuUcUmiju10hR2jEZHiQd/nXULKRfosXZsGTQNBHiKXl+mNxvDQo5gse0gZgO+m61MLjG1Xd+okiYemKhp4zFcvERjB+VX499UkeTGq2Zx9d6+8TWANZitJiYPyWdEXEGZdSjQMetO+MX0lpwmOS3AUF8b70p5JjDWXES+w1oMjzo7mhQnB4FU5HaeNcKS/E+JT3XZrIQRq6Yqy5SlX6oRRgsjMCPEb1BXI70X+qrHlRuysg/TMrDI9+hrLy1vGbOE/9ADnO+eSaOyVj2aKXdfBiT4/pU4iM+w+VNebGzx64A6KEA/8Qf5pbbnDrvjemwJLGifIp1kZn9Fk0a9LafzY2rTp8atZuOcOfl1bFoVWU9Zevy/moi9uUjfQhBY+IORVdNkNnjFVO7EGtV7KsyoHb7gzn22oKfWWBmJAPTFa5AZCFY6VHRR40ykPUMbq/hurW3jEITSpGcDp4H3ronJvFoG5ft42kzJCulh4iuWG30RCXWuhjtvutVfJE0BuHXGI2jUavzP4mo8jHNR3NfFtq/6dAe+LqSkLEGpvjDFrvUFIyozT2OeW3idHwyp5eVJ+NOe1jdDjINS4y1taL8rwLgazB23rSgIzk5rZ1rYeezZyO2uyvogQMyqTn2o7mtccKtlyD3z0pfyfA54dNKiE6pcE+wo/mY4sLSOSNgCxwaDEI25XvQj+v+KteT2Q8PkiK6j2jfvUbcAdvbjP4qO4bzMvA5JIJLVnWQlw2rAz0z0+FRzw7nSL8W5jJujLnG0eLjkjktplRWU+gGP4pFLNHbqSzUZzJzBHxWdLtYOxYJ2eNerVv7bdaQCWZiWABz1J/imxS1C2Lladto3SXE9ymYz3B4DrQ0kpyAQB8KZXXEu2tLaOODR2GA2CMKAMbDGd+pyTvS6YCXtHHXrVkRN8ZZ4CCSyjw8RWmVSVGN/WjLZAUXG2R+tYlNLshooAFgYAaug8o8OjblxX0bszNkdeux/TFQaxmW4WADdzj29aveAcTj4eRaykCOTATJAxgYqHKT6PibOHpXuh3cvJ2Ucy94FcNS28lE0MWD3lYinVkNcckZU6QT60rv4FjZWQYBqPHflM1cqXraAFU+FZAHyrdEmckda90EDNajzfZu5PV4+CdodlaZunpXnOWfo1iQ251Gs+UbjsuW4lAXeVySRt1rRzvchUsdeB3W6bURNkjNKfpEGfA0Lxte1tIJ0bvIzgjxwaxvLlRcRad6+e5VrdUOQNRyRR0cLrTM7dmyhiBsKKOqOMxtjUu49qDRmguxv6H2p9aPbRT9tdxdogjYDKB9JI2YqSAwHlSsICsLZ1KoD4+DCg7hdLtp2yNwfCnDTw3M7zWsZihZjoQ/hFBcUTuh8jPtRXkBlbN9nHitk6faavOhrJtSqPEUVc/dzTewB3L/D/AKZNczEf4YCJ7nc/3619xuykWFUdW3OxAz8qb8oxj6oEiqAzyPqI6sdRG/ypPzTcM9yuNLRglVDLtkHBwfPIqSpuzU1M4kIrK5nt5ykM08JG32UrLim/DOLXp4lFHc3c8sQycSNnOx86SyNrkV1ZcnyyT86acNgHbdqW1aPM+dV0QVNFpBcxEZyM1uRkfqam9RU5U4HlRFvekbHNLo7Y+5UeIcAhWQbuzFTn1oD/AGhQHNkBk/Zk/tQXLcwli4dGJAAE7wx45p3zuoa5tF/LFRFOczW7rIhbqRnFa5+7bK2MZfFO7qIPeR7baDilfEYtNhAPEyMceNcA0zwgyah+JPmKKtSXtwG64wa0ltQiPof2p1bScMHADEUAv9eScHJOoYwemnTnbrmlbChbZrojIrXxPe39q3rsa1Xw1W5FFeQmnhyd3VRNwCYzXloumEbVm4yNzTexRtwK+Fny/KwI7RJmRB/Udx/J+FJ+Nk/Y2+osIkAyepPUmhJZTa3VuWJMKyiRk8z0/at0xE9w0gxpJpenTHdbSRgkTSxRg5eRmACnB1HwFU31QLS1VEIL4HaHzOKH5asjLcfSnH2UO0fq/n8KppVQg53o7BrsSbIyNgGsAWU03u7NHYlc5pZJG0RIYVwp/9k="
                alt=""
              />
            </a>
            <a className="text-white font-extrabold ml-4" href="#">
              <p className="text_name text-xs">Liza Koshy</p>
            </a>
          </div>
          <div className="item_performer flex items-center mb-5">
            <a href="#">
              <img
                className="w-28 h-32 object-cover rounded-lg"
                src="http://buster.mbkip3ms9u-e92498n216kr.p.temp-site.link/wp-content/uploads/2020/05/son-tung-mtp.png"
                alt=""
              />
            </a>
            <a className="text-white font-extrabold ml-4" href="#">
              <p className="text_name text-xs">Sơn Tùng - MTP</p>
            </a>
          </div>
          <div className="view_all_btn ">
            <div>
              <a className="underline_item" href="">
                SEE ALL CELEBRITIES
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="empty_space h-28"></div>
      <div className="container">
        <Content />
      </div>
    </div>
  );
}
