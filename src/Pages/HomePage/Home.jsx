import React from "react";
import Search from "../../Components/Search/Search";
import "./Home.css";
import SimpleSlider from "../../Components/Slick/Slick";
import ListMovie from "./ListMovie/ListMovie";

export default function Home() {
  return (
    <div className="home_page">
      <div className="container">
        <Search />
      </div>
      <div >
        <ListMovie/>
      </div>
    </div>
  );
}
