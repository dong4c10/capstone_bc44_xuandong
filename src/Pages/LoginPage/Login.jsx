import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { https } from "../../Services/config";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Lottie from "lottie-react";
import bgAnimation from "./animation_ljzua8fu.json";
import "./Login.css";
import { setLogin } from "../../redux/spinnerSlice";
import { localServ } from "../../Services/LocalStoreService";
import { faUser } from "@fortawesome/free-solid-svg-icons";

const onFinishFailed = (errorInfo) => {
  console.log("Failed:", errorInfo);
};

export default function Login() {
  let dispatch = useDispatch();
  let navigate = useNavigate();

  const onFinish = (values) => {
    console.log("value: ", values);
    https
      .post("/api/QuanLyNguoiDung/DangNhap", values)
      .then((res) => {
        console.log("res: ", res);
        message.success("Đăng nhập thành công");
        //đẩy data lên redux =
        dispatch(setLogin(res.data.content));
        localServ.setUser(res.data.content);
        setTimeout(() => {
          navigate("/");
        }, 2000);
      })
      .catch((error) => {
        console.log("error: ", error);
        message.error("Đăng nhập thất bại");
      });
  };
  return (
    <div className="Main_login h-screen flex items-center ">
      <div className="container p-10 bg-white rounded flex items-center">
        <div className="w-1/2">
          <Lottie animationData={bgAnimation} />
        </div>

        <div className="w-1/2 text-right">
          <div className=" bg-orange-600 flex text-center items-center m-auto justify-center w-24 h-24 rounded-full">
            <FontAwesomeIcon className="text-5xl" icon={faUser} />
          </div>
          <div className="text-black mt-6 text-2xl text-center">
            <h2 className="font-bold ">Đăng Nhập</h2>
          </div>

          <Form
            className="mt-9"
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tên tài khoản của bạn!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập mật khẩu của bạn!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Button
                className="bg-orange-400 font-bold hover:bg-white"
                htmlType="submit"
              >
                Đăng Nhập
              </Button>
            </Form.Item>

            <a className="text-gray-700" href="">
              Bạn Chưa Có Tài? Đăng Ký Ngay{" "}
            </a>
          </Form>
        </div>
      </div>
    </div>
  );
}
